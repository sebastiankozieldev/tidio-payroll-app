<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Infrastructure\Laravel\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    public function run(): void
    {
        Department::factory()->constantSalaryBonus()->create([
            'name' => 'HR Department',
            'constant_salary_bonus' => 10000, //100.00
        ]);

        Department::factory()->constantSalaryBonus()->create([
            'name' => 'QA Department',
            'constant_salary_bonus' => 5000, //50.00
        ]);

        Department::factory()->constantSalaryBonus()->create([
            'name' => 'PM Department',
            'constant_salary_bonus' => 20000, //200.00
        ]);

        Department::factory()->percentageSalaryBonus()->create([
            'name' => 'PHP Department',
            'percentage_of_salary_bonus' => 10.0,
            'percentage_salary_bonus_threshold' => 5,
        ]);
        Department::factory()->percentageSalaryBonus()->create([
            'name' => 'Node Department',
            'percentage_of_salary_bonus' => 5.0,
            'percentage_salary_bonus_threshold' => 1,
        ]);

        Department::factory()->percentageSalaryBonus()->create([
            'name' => 'Frontend Department',
            'percentage_of_salary_bonus' => 7.5,
            'percentage_salary_bonus_threshold' => 3,
        ]);
    }
}
