<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Infrastructure\Laravel\Models\{Department, Employee};
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    public function run(): void
    {
        Department::all()->each(function (Department $department): void {
            Employee::factory()
                ->count(5)
                ->for($department)
                ->create();
        });
    }
}
