<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('employees', function (Blueprint $table): void {
            $table->uuid('id')->primary();
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('base_salary');
            $table->uuid('department_id');
            $table->foreign('department_id')->references('id')->on('departments')
                ->onDelete('restrict');
            $table->date('employment_started_at');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::table('employees', function(Blueprint $table): void {
            $table->dropForeign(['department_id']);
        });

        Schema::dropIfExists('employees');
    }
};
