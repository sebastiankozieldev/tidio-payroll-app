<?php

declare(strict_types=1);

namespace Database\Factories\Infrastructure\Laravel\Models;

use App\Infrastructure\Laravel\Models\Employee;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Infrastructure\Laravel\Models\Employee>
 */
final class EmployeeFactory extends Factory
{
    protected $model = Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $yearsOfService = $this->faker->numberBetween(0, 15);

        return [
            'id' => $this->faker->uuid,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'base_salary' => $this->faker->randomElement([
                50000, //500.00
                100000, //1000.00
                200000, //2000.00
                300000, //3000.00
                350000, //3500.00
                400000, //4000.00
            ]),
            'employment_started_at' => now()->subYears($yearsOfService),
        ];
    }
}
