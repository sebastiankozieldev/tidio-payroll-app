<?php

declare(strict_types=1);

namespace Database\Factories\Infrastructure\Laravel\Models;

use App\Domain\Department\Model\SalaryBonusType;
use App\Infrastructure\Laravel\Models\Department;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Infrastructure\Laravel\Models\Department>
 */
final class DepartmentFactory extends Factory
{
    protected $model = Department::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id' => $this->faker->uuid,
            'name' => $this->faker->words(3, true),
        ];
    }

    public function constantSalaryBonus(): self
    {
        return $this->state(function (): array {
            return [
                'salary_bonus_type' => SalaryBonusType::CONSTANT->value,
                'constant_salary_bonus' => $this->faker->randomNumber(),
            ];
        });
    }

    public function percentageSalaryBonus(): self
    {
        return $this->state(function (): array {
            return [
                'salary_bonus_type' => SalaryBonusType::PERCENTAGE->value,
                'percentage_of_salary_bonus' => (float) $this->faker->randomDigit(),
                'percentage_salary_bonus_threshold' => $this->faker->randomDigit(),
            ];
        });
    }
}
