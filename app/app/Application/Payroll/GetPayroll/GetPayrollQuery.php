<?php

declare(strict_types=1);

namespace App\Application\Payroll\GetPayroll;

use App\Application\Query\{Filter, OrderDirection};

final class GetPayrollQuery
{
    /**
     * @var Filter[]
     */
    private array $filters;

    private function __construct(
        private PayrollOrderBy $payrollOrderBy,
        private OrderDirection $orderDirection,
        Filter ...$filters
    ) {
        $this->filters = $filters;
    }

    public static function create(
        PayrollOrderBy $payrollOrderBy,
        OrderDirection $orderDirection,
        Filter ...$filters
    ): self {
        return new self($payrollOrderBy, $orderDirection, ...$filters);
    }

    public function getPayrollOrderBy(): PayrollOrderBy
    {
        return $this->payrollOrderBy;
    }

    public function getOrderDirection(): OrderDirection
    {
        return $this->orderDirection;
    }

    /**
     * @return Filter[]
     */
    public function getFilters(): array
    {
        return $this->filters;
    }
}
