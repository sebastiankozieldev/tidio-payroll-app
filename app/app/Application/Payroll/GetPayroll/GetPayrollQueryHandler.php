<?php

declare(strict_types=1);

namespace App\Application\Payroll\GetPayroll;

use App\Application\Payroll\Model\Payroll;

interface GetPayrollQueryHandler
{
    public function get(GetPayrollQuery $getPayrollQuery): Payroll;
}
