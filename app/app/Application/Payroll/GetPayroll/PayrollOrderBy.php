<?php

declare(strict_types=1);

namespace App\Application\Payroll\GetPayroll;

enum PayrollOrderBy: string
{
    case FIRST_NAME = 'first_name';
    case LAST_NAME = 'last_name';
    case DEPARTMENT_NAME = 'department_name';
    case BASE_SALARY = 'base_salary';
    case SALARY_BONUS = 'salary_bonus';
    case SALARY_BONUS_TYPE = 'salary_bonus_type';
    case TOTAL_SALARY = 'total_salary';
}
