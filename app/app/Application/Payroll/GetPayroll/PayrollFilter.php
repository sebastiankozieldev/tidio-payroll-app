<?php

declare(strict_types=1);

namespace App\Application\Payroll\GetPayroll;

enum PayrollFilter: string
{
    case DEPARTMENT_NAME = 'department_name';
    case FIRST_NAME = 'first_name';
    case LAST_NAME = 'last_name';
}
