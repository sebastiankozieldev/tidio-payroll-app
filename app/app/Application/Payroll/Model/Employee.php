<?php

declare(strict_types=1);

namespace App\Application\Payroll\Model;

use App\Domain\Department\Model\Name;
use App\Domain\Employee\Model\{FirstName, LastName};
use App\Domain\Payroll\Model\Salary;

final class Employee
{
    private function __construct(
        private FirstName $firstName,
        private LastName $lastName,
        private Name $name,
        private Salary $salary
    ) {
    }

    public static function create(FirstName $firstName, LastName $lastName, Name $name, Salary $salary): self
    {
        return new self($firstName, $lastName, $name, $salary);
    }

    public function getFirstName(): FirstName
    {
        return $this->firstName;
    }

    public function getLastName(): LastName
    {
        return $this->lastName;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getSalary(): Salary
    {
        return $this->salary;
    }
}
