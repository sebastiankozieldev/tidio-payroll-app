<?php

declare(strict_types=1);

namespace App\Application\Payroll\Model;

final class Payroll
{
    /**
     * @var Employee[]
     */
    private array $employees;

    private function __construct(Employee ...$employees)
    {
        $this->employees = $employees;
    }

    public static function create(Employee ...$employees): self
    {
        return new self(...$employees);
    }

    /**
     * @return Employee[]
     */
    public function getEmployees(): array
    {
        return $this->employees;
    }
}
