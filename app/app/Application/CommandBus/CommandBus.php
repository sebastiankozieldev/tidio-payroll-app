<?php

declare(strict_types=1);

namespace App\Application\CommandBus;

use App\Application\CommandBus\Exception\MissingHandlerException;

interface CommandBus
{
    /**
     * @throws MissingHandlerException
     */
    public function dispatch(Command $command): void;

    /**
     * @param string[] $map mapping of commands and handlers, example
     *                      [
     *                      CommandA::class => HandlerA::class,
     *                      CommandB::class => HandlerB::class,
     *                      ]
     */
    public function map(array $map): void;
}
