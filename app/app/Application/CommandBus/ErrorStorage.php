<?php

declare(strict_types=1);

namespace App\Application\CommandBus;

/**
 * Stores errors occurred during command handling.
 */
interface ErrorStorage
{
    /**
     * @return string[]
     */
    public function getErrors(): array;

    public function setErrors(string ...$errors): void;

    public function addError(string $error): void;

    public function clear(): void;
}
