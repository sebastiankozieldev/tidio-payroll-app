<?php

declare(strict_types=1);

namespace App\Application\CommandBus\Exception;

use App\Application\CommandBus\Command;
use Exception;

final class MissingHandlerException extends Exception
{
    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function create(Command $command): self
    {
        return new self(sprintf("Missing handler for command: '%s'", $command::class));
    }
}
