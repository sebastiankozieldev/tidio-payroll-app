<?php

declare(strict_types=1);

namespace App\Application\CommandBus;

final class InMemoryErrorStorage implements ErrorStorage
{
    /**
     * @param string[] $errors
     */
    public function __construct(private array $errors = [])
    {
    }

    /**
     * @return string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    public function setErrors(string ...$errors): void
    {
        $this->errors = $errors;
    }

    public function addError(string $error): void
    {
        $this->errors[] = $error;
    }

    public function clear(): void
    {
        $this->errors = [];
    }
}
