<?php

declare(strict_types=1);

namespace App\Application\Employee\CreateEmployee;

use App\Application\CommandBus\ErrorStorage;
use App\Application\Transaction\Transaction;
use App\Domain\Common\Id\Employee\EmployeeIdGenerator;
use App\Domain\Employee\Model\Employee;
use App\Domain\Employee\Repository\EmployeeRepository;
use App\Domain\Employee\Repository\Exception\UnableToSaveEmployeeException;

class CreateEmployeeHandler
{
    public function __construct(
        private EmployeeRepository $employeeRepository,
        private EmployeeIdGenerator $employeeIdGenerator,
        private Transaction $transaction,
        private ErrorStorage $errorStorage
    ) {
    }

    public function __invoke(CreateEmployee $command): void
    {
        $employee = Employee::create(
            $this->employeeIdGenerator->generate(),
            $command->getFirstName(),
            $command->getLastName(),
            $command->getBaseSalary(),
            $command->getEmploymentStartDate(),
            $command->getDepartmentId()
        );

        $this->transaction->begin();

        try {
            $this->employeeRepository->save($employee);
        } catch (UnableToSaveEmployeeException $exception) {
            $this->errorStorage->addError($exception->getMessage());
            $this->transaction->rollBack();

            return;
        }

        $this->transaction->commit();
    }
}
