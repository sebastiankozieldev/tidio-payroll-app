<?php

declare(strict_types=1);

namespace App\Application\Employee\CreateEmployee;

use App\Application\CommandBus\Command;
use App\Domain\Common\Id\Department\DepartmentId;
use App\Domain\Common\Model\BaseSalary;
use App\Domain\Employee\Model\{FirstName, LastName};
use DateTimeImmutable;

final class CreateEmployee implements Command
{
    private function __construct(
        private FirstName $firstName,
        private LastName $lastName,
        private BaseSalary $baseSalary,
        private DateTimeImmutable $employmentStartDate,
        private DepartmentId $departmentId
    ) {
    }

    public static function create(
        FirstName $firstName,
        LastName $lastName,
        BaseSalary $baseSalary,
        DateTimeImmutable $employmentStartDate,
        DepartmentId $departmentId
    ): self {
        return new self($firstName, $lastName, $baseSalary, $employmentStartDate, $departmentId);
    }

    public function getFirstName(): FirstName
    {
        return $this->firstName;
    }

    public function getLastName(): LastName
    {
        return $this->lastName;
    }

    public function getBaseSalary(): BaseSalary
    {
        return $this->baseSalary;
    }

    public function getEmploymentStartDate(): DateTimeImmutable
    {
        return $this->employmentStartDate;
    }

    public function getDepartmentId(): DepartmentId
    {
        return $this->departmentId;
    }
}
