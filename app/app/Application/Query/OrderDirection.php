<?php

declare(strict_types=1);

namespace App\Application\Query;

enum OrderDirection: string
{
    case ASC = 'asc';
    case DESC = 'desc';
}
