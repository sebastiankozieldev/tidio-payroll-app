<?php

declare(strict_types=1);

namespace App\Application\Query;

final class Filter
{
    private function __construct(
        private string $field,
        private string $value
    ) {
    }

    public static function create(string $field, string $value): self
    {
        return new self($field, $value);
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
