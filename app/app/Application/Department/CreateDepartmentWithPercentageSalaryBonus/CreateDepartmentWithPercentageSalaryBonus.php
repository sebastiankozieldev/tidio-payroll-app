<?php

declare(strict_types=1);

namespace App\Application\Department\CreateDepartmentWithPercentageSalaryBonus;

use App\Application\CommandBus\Command;
use App\Domain\Common\Model\{PercentageOfSalaryBonus, PercentageSalaryBonusThreshold};
use App\Domain\Department\Model\Name;

final class CreateDepartmentWithPercentageSalaryBonus implements Command
{
    private function __construct(
        private Name $departmentName,
        private PercentageOfSalaryBonus $percentageOfSalaryBonus,
        private PercentageSalaryBonusThreshold $percentageSalaryBonusThreshold
    ) {
    }

    public static function create(
        Name $departmentName,
        PercentageOfSalaryBonus $percentageOfSalaryBonus,
        PercentageSalaryBonusThreshold $percentageSalaryBonusThreshold
    ): self {
        return new self($departmentName, $percentageOfSalaryBonus, $percentageSalaryBonusThreshold);
    }

    public function getDepartmentName(): Name
    {
        return $this->departmentName;
    }

    public function getPercentageOfSalaryBonus(): PercentageOfSalaryBonus
    {
        return $this->percentageOfSalaryBonus;
    }

    public function getPercentageSalaryBonusThreshold(): PercentageSalaryBonusThreshold
    {
        return $this->percentageSalaryBonusThreshold;
    }
}
