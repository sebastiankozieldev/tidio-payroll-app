<?php

declare(strict_types=1);

namespace App\Application\Department\CreateDepartmentWithConstantSalaryBonus;

use App\Application\CommandBus\ErrorStorage;
use App\Application\Transaction\Transaction;
use App\Domain\Common\Id\Department\DepartmentIdGenerator;
use App\Domain\Department\Model\{Department};
use App\Domain\Department\Repository\DepartmentRepository;
use App\Domain\Department\Repository\Exception\UnableToSaveDepartmentException;

class CreateDepartmentWithConstantSalaryBonusHandler
{
    public function __construct(
        private DepartmentRepository $departmentRepository,
        private DepartmentIdGenerator $departmentIdGenerator,
        private Transaction $transaction,
        private ErrorStorage $errorStorage
    ) {
    }

    public function __invoke(CreateDepartmentWithConstantSalaryBonus $command): void
    {
        $departmentName = $command->getDepartmentName();

        $departmentNameTaken = $this->departmentRepository->existsWithName($departmentName);

        if ($departmentNameTaken === true) {
            $this->errorStorage->addError(sprintf("Department name: '%s' is taken. Try another", $departmentName));

            return;
        }

        $departmentId = $this->departmentIdGenerator->generate();

        $department = Department::createWithConstantBonus(
            $departmentId,
            $departmentName,
            $command->getConstantSalaryBonus()
        );

        $this->transaction->begin();

        try {
            $this->departmentRepository->save($department);
        } catch (UnableToSaveDepartmentException $exception) {
            $this->errorStorage->addError($exception->getMessage());
            $this->transaction->rollBack();

            return;
        }

        $this->transaction->commit();
    }
}
