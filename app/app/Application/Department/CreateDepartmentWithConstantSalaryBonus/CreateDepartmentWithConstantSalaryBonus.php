<?php

declare(strict_types=1);

namespace App\Application\Department\CreateDepartmentWithConstantSalaryBonus;

use App\Application\CommandBus\Command;
use App\Domain\Common\Model\ConstantSalaryBonus;
use App\Domain\Department\Model\{Name};

final class CreateDepartmentWithConstantSalaryBonus implements Command
{
    private function __construct(
        private Name $departmentName,
        private ConstantSalaryBonus $constantSalaryBonus
    ) {
    }

    public static function create(Name $departmentName, ConstantSalaryBonus $constantSalaryBonus): self
    {
        return new self($departmentName, $constantSalaryBonus);
    }

    public function getDepartmentName(): Name
    {
        return $this->departmentName;
    }

    public function getConstantSalaryBonus(): ConstantSalaryBonus
    {
        return $this->constantSalaryBonus;
    }
}
