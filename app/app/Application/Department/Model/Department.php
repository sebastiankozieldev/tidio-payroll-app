<?php

declare(strict_types=1);

namespace App\Application\Department\Model;

use App\Domain\Common\Id\Department\DepartmentId;
use App\Domain\Department\Model\Name;

final class Department
{
    private function __construct(
        private DepartmentId $departmentId,
        private Name $name
    ) {
    }

    public static function create(DepartmentId $departmentId, Name $name): self
    {
        return new self($departmentId, $name);
    }

    public function getDepartmentId(): DepartmentId
    {
        return $this->departmentId;
    }

    public function getName(): Name
    {
        return $this->name;
    }
}
