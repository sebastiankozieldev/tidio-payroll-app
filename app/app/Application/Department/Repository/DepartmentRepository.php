<?php

declare(strict_types=1);

namespace App\Application\Department\Repository;

use App\Application\Department\Model\Department;

interface DepartmentRepository
{
    /**
     * @return Department[]
     */
    public function findAll(): array;
}
