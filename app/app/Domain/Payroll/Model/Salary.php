<?php

declare(strict_types=1);

namespace App\Domain\Payroll\Model;

use App\Domain\Common\Model\{BaseSalary, SalaryBonus};
use App\Domain\Department\Model\SalaryBonusType;

final class Salary
{
    private int $amount;

    private function __construct(
        private BaseSalary $baseSalary,
        private SalaryBonus $salaryBonus,
        private SalaryBonusType $salaryBonusType
    ) {
        $this->amount = $baseSalary->getAmount() + $salaryBonus->getAmount();
    }

    public static function create(
        BaseSalary $baseSalary,
        SalaryBonus $salaryBonus,
        SalaryBonusType $salaryBonusType
    ): self {
        return new self($baseSalary, $salaryBonus, $salaryBonusType);
    }

    public function getBaseSalary(): BaseSalary
    {
        return $this->baseSalary;
    }

    public function getSalaryBonus(): SalaryBonus
    {
        return $this->salaryBonus;
    }

    public function getSalaryBonusType(): SalaryBonusType
    {
        return $this->salaryBonusType;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }
}
