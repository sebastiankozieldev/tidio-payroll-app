<?php

declare(strict_types=1);

namespace App\Domain\Payroll\Calculator;

use App\Domain\Common\Model\{ConstantSalaryBonus, SalaryBonus, YearsOfService};

final class ConstantSalaryBonusCalculator implements SalaryBonusCalculator
{
    private const MAX_YEARS_OF_SERVICE_AFFECTING_SALARY_BONUS = 10;

    public function __construct(
        private YearsOfService $yearsOfService,
        private ConstantSalaryBonus $constantSalaryBonus
    ) {
    }

    public function calculate(): SalaryBonus
    {
        $yearsOfService = $this->yearsOfService->getYears();
        if ($yearsOfService > self::MAX_YEARS_OF_SERVICE_AFFECTING_SALARY_BONUS) {
            $yearsOfService = self::MAX_YEARS_OF_SERVICE_AFFECTING_SALARY_BONUS;
        }

        $constantSalaryBonus = $this->constantSalaryBonus->getAmount();

        return SalaryBonus::create($yearsOfService * $constantSalaryBonus);
    }
}
