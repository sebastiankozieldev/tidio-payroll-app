<?php

declare(strict_types=1);

namespace App\Domain\Payroll\Calculator;

use App\Domain\Common\Model\{BaseSalary, PercentageOfSalaryBonus, PercentageSalaryBonusThreshold, SalaryBonus, YearsOfService};

final class PercentageSalaryBonusCalculator implements SalaryBonusCalculator
{
    public function __construct(
        private BaseSalary $baseSalary,
        private YearsOfService $yearsOfService,
        private PercentageOfSalaryBonus $percentageOfSalaryBonus,
        private PercentageSalaryBonusThreshold $percentageSalaryBonusThreshold
    ) {
    }

    public function calculate(): SalaryBonus
    {
        $yearsOfService = $this->yearsOfService->getYears();
        $percentageSalaryBonusThreshold = $this->percentageSalaryBonusThreshold->getYears();

        if ($yearsOfService < $percentageSalaryBonusThreshold) {
            return SalaryBonus::create(0);
        }

        $baseSalary = $this->baseSalary->getAmount();
        $percentageOfSalaryBonus = $this->percentageOfSalaryBonus->getPercentage();

        $salaryBonus = (int) round($baseSalary * $percentageOfSalaryBonus / 100);

        return SalaryBonus::create($salaryBonus);
    }
}
