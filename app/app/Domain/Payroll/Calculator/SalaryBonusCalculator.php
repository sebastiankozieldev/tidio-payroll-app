<?php

declare(strict_types=1);

namespace App\Domain\Payroll\Calculator;

use App\Domain\Common\Model\SalaryBonus;

interface SalaryBonusCalculator
{
    public function calculate(): SalaryBonus;
}
