<?php

declare(strict_types=1);

namespace App\Domain\Payroll\Factory;

use App\Domain\Common\Model\{BaseSalary, ConstantSalaryBonus, PercentageOfSalaryBonus, PercentageSalaryBonusThreshold, YearsOfService};
use App\Domain\Department\Model\SalaryBonusType;
use App\Domain\Payroll\Calculator\{ConstantSalaryBonusCalculator, PercentageSalaryBonusCalculator};
use App\Domain\Payroll\Model\Salary;
use App\Utils\DateTime\DateTimeProvider;
use DateTimeImmutable;

class SalaryFactory
{
    public function __construct(private DateTimeProvider $dateTimeProvider)
    {
    }

    public function makeSalaryWithConstantSalaryBonus(
        BaseSalary $baseSalary,
        DateTimeImmutable $employmentStartDate,
        ConstantSalaryBonus $constantSalaryBonus
    ): Salary {
        $yearsOfService = $this->makeYearsOfService($employmentStartDate);
        $salaryBonus = (new ConstantSalaryBonusCalculator($yearsOfService, $constantSalaryBonus))->calculate();

        return Salary::create($baseSalary, $salaryBonus, SalaryBonusType::CONSTANT);
    }

    public function makeSalaryWithPercentageSalaryBonus(
        BaseSalary $baseSalary,
        DateTimeImmutable $employmentStartDate,
        PercentageOfSalaryBonus $percentageOfSalaryBonus,
        PercentageSalaryBonusThreshold $percentageSalaryBonusThreshold
    ): Salary {
        $yearsOfService = $this->makeYearsOfService($employmentStartDate);
        $salaryBonus = (new PercentageSalaryBonusCalculator(
            $baseSalary,
            $yearsOfService,
            $percentageOfSalaryBonus,
            $percentageSalaryBonusThreshold
        ))->calculate();

        return Salary::create($baseSalary, $salaryBonus, SalaryBonusType::PERCENTAGE);
    }

    private function makeYearsOfService(DateTimeImmutable $employmentStartDate): YearsOfService
    {
        $diff = $this->dateTimeProvider->now()->diff($employmentStartDate);

        return YearsOfService::create($diff->y);
    }
}
