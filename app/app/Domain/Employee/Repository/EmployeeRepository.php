<?php

declare(strict_types=1);

namespace App\Domain\Employee\Repository;

use App\Domain\Employee\Model\Employee;
use App\Domain\Employee\Repository\Exception\UnableToSaveEmployeeException;

interface EmployeeRepository
{
    /**
     * @throws UnableToSaveEmployeeException
     */
    public function save(Employee $employee): void;
}
