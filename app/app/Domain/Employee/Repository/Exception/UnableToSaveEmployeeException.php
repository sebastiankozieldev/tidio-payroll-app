<?php

declare(strict_types=1);

namespace App\Domain\Employee\Repository\Exception;

use App\Domain\Common\Id\Employee\EmployeeId;
use Exception;
use Throwable;

final class UnableToSaveEmployeeException extends Exception
{
    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function create(EmployeeId $employeeId, Throwable $previous): self
    {
        return new self(sprintf(
            "Unable to save employee with id: '%s'. Reason: '%s",
            $employeeId,
            $previous->getMessage()
        ));
    }
}
