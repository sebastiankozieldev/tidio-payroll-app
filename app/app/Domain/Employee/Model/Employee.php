<?php

declare(strict_types=1);

namespace App\Domain\Employee\Model;

use App\Domain\Common\Id\Department\DepartmentId;
use App\Domain\Common\Id\Employee\EmployeeId;
use App\Domain\Common\Model\BaseSalary;
use DateTimeImmutable;

final class Employee
{
    private function __construct(
        private EmployeeId $employeeId,
        private FirstName $firstName,
        private LastName $lastName,
        private BaseSalary $baseSalary,
        private DateTimeImmutable $employmentStartDate,
        private DepartmentId $departmentId
    ) {
    }

    public static function create(
        EmployeeId $employeeId,
        FirstName $firstName,
        LastName $lastName,
        BaseSalary $baseSalary,
        DateTimeImmutable $employmentStartDate,
        DepartmentId $departmentId
    ): self {
        return new self($employeeId, $firstName, $lastName, $baseSalary, $employmentStartDate, $departmentId);
    }

    public function getEmployeeId(): EmployeeId
    {
        return $this->employeeId;
    }

    public function getFirstName(): FirstName
    {
        return $this->firstName;
    }

    public function getLastName(): LastName
    {
        return $this->lastName;
    }

    public function getBaseSalary(): BaseSalary
    {
        return $this->baseSalary;
    }

    public function getEmploymentStartDate(): DateTimeImmutable
    {
        return $this->employmentStartDate;
    }

    public function getDepartmentId(): DepartmentId
    {
        return $this->departmentId;
    }
}
