<?php

declare(strict_types=1);

namespace App\Domain\Employee\Model;

use InvalidArgumentException;

final class LastName
{
    /**
     * @throws InvalidArgumentException
     */
    private function __construct(private string $lastName)
    {
        if (trim($this->lastName) === '') {
            throw new InvalidArgumentException('Last name cannot be empty');
        }
    }

    public function __toString(): string
    {
        return $this->lastName;
    }

    /**
     * @throws InvalidArgumentException
     */
    public static function create(string $lastName): self
    {
        return new self($lastName);
    }
}
