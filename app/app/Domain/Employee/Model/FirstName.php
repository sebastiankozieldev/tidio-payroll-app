<?php

declare(strict_types=1);

namespace App\Domain\Employee\Model;

use InvalidArgumentException;

final class FirstName
{
    /**
     * @throws InvalidArgumentException
     */
    private function __construct(private string $employeeFirstName)
    {
        if (trim($this->employeeFirstName) === '') {
            throw new InvalidArgumentException('Employee first name cannot be empty');
        }
    }

    public function __toString(): string
    {
        return $this->employeeFirstName;
    }

    /**
     * @throws InvalidArgumentException
     */
    public static function create(string $employeeFirstName): self
    {
        return new self($employeeFirstName);
    }
}
