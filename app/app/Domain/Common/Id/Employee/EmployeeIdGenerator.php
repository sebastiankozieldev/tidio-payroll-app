<?php

declare(strict_types=1);

namespace App\Domain\Common\Id\Employee;

interface EmployeeIdGenerator
{
    public function generate(): EmployeeId;
}
