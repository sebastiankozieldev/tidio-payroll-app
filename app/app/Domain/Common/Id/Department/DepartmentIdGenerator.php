<?php

declare(strict_types=1);

namespace App\Domain\Common\Id\Department;

interface DepartmentIdGenerator
{
    public function generate(): DepartmentId;
}
