<?php

declare(strict_types=1);

namespace App\Domain\Common\Id\Department;

interface DepartmentId
{
    public function __toString(): string;

    public static function from(string $id): self;

    public static function create(): self;
}
