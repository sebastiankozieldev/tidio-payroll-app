<?php

declare(strict_types=1);

namespace App\Domain\Common\Model;

use InvalidArgumentException;

final class PercentageOfSalaryBonus
{
    /**
     * @throws InvalidArgumentException
     */
    private function __construct(private float $percentage)
    {
        if ($this->percentage < 0) {
            throw new InvalidArgumentException('Percentage of salary bonus cannot be negative value');
        }
    }

    /**
     * @throws InvalidArgumentException
     */
    public static function create(float $percentage): self
    {
        return new self($percentage);
    }

    public function getPercentage(): float
    {
        return $this->percentage;
    }
}
