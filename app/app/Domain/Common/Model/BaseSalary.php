<?php

declare(strict_types=1);

namespace App\Domain\Common\Model;

use InvalidArgumentException;

final class BaseSalary
{
    /**
     * @throws InvalidArgumentException
     */
    private function __construct(private int $amount)
    {
        if ($this->amount < 0) {
            throw new InvalidArgumentException('Base salary amount cannot be negative value');
        }
    }

    /**
     * @throws InvalidArgumentException
     */
    public static function create(int $amount): self
    {
        return new self($amount);
    }

    public function getAmount(): int
    {
        return $this->amount;
    }
}
