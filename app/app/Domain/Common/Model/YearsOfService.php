<?php

declare(strict_types=1);

namespace App\Domain\Common\Model;

use InvalidArgumentException;

final class YearsOfService
{
    /**
     * @throws InvalidArgumentException
     */
    private function __construct(private int $years)
    {
        if ($this->years < 0) {
            throw new InvalidArgumentException('Years of service cannot be negative value');
        }
    }

    /**
     * @throws InvalidArgumentException
     */
    public static function create(int $years): self
    {
        return new self($years);
    }

    public function getYears(): int
    {
        return $this->years;
    }
}
