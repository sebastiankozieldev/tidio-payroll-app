<?php

declare(strict_types=1);

namespace App\Domain\Department\Repository;

use App\Domain\Department\Model\{Department, Name};
use App\Domain\Department\Repository\Exception\UnableToSaveDepartmentException;

interface DepartmentRepository
{
    /**
     * @throws UnableToSaveDepartmentException
     */
    public function save(Department $department): void;

    public function existsWithName(Name $departmentName): bool;
}
