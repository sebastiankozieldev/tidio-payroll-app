<?php

declare(strict_types=1);

namespace App\Domain\Department\Repository\Exception;

use App\Domain\Common\Id\Department\DepartmentId;
use Exception;
use Throwable;

final class UnableToSaveDepartmentException extends Exception
{
    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function create(DepartmentId $departmentId, Throwable $previous): self
    {
        return new self(sprintf(
            "Unable to save department with id: '%s'. Reason: '%s",
            $departmentId,
            $previous->getMessage()
        ));
    }
}
