<?php

declare(strict_types=1);

namespace App\Domain\Department\Model;

enum SalaryBonusType: string
{
    case PERCENTAGE = 'percentage';
    case CONSTANT = 'constant';
}
