<?php

declare(strict_types=1);

namespace App\Domain\Department\Model;

use App\Domain\Common\Id\Department\DepartmentId;
use App\Domain\Common\Model\{ConstantSalaryBonus, PercentageOfSalaryBonus, PercentageSalaryBonusThreshold};
use InvalidArgumentException;

final class Department
{
    /**
     * @throws InvalidArgumentException
     */
    private function __construct(
        private DepartmentId $departmentId,
        private Name $name,
        private SalaryBonusType $salaryBonusType,
        private ?ConstantSalaryBonus $constantSalaryBonus = null,
        private ?PercentageOfSalaryBonus $percentageOfSalaryBonus = null,
        private ?PercentageSalaryBonusThreshold $percentageSalaryBonusThreshold = null,
    ) {
        $this->guardConstantSalaryBonus();
        $this->guardPercentageOfSalaryBonusAndPercentageSalaryBonusThreshold();
    }

    public static function createWithPercentageBonus(
        DepartmentId $departmentId,
        Name $name,
        PercentageOfSalaryBonus $percentageOfSalaryBonus,
        PercentageSalaryBonusThreshold $percentageSalaryBonusThreshold
    ): self {
        return new self(
            $departmentId,
            $name,
            SalaryBonusType::PERCENTAGE,
            null,
            $percentageOfSalaryBonus,
            $percentageSalaryBonusThreshold
        );
    }

    public static function createWithConstantBonus(
        DepartmentId $departmentId,
        Name $name,
        ConstantSalaryBonus $constantSalaryBonus,
    ): self {
        return new self($departmentId, $name, SalaryBonusType::CONSTANT, $constantSalaryBonus);
    }

    public function getDepartmentId(): DepartmentId
    {
        return $this->departmentId;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getSalaryBonusType(): SalaryBonusType
    {
        return $this->salaryBonusType;
    }

    public function getConstantSalaryBonus(): ?ConstantSalaryBonus
    {
        return $this->constantSalaryBonus;
    }

    public function getPercentageOfSalaryBonus(): ?PercentageOfSalaryBonus
    {
        return $this->percentageOfSalaryBonus;
    }

    public function getPercentageSalaryBonusThreshold(): ?PercentageSalaryBonusThreshold
    {
        return $this->percentageSalaryBonusThreshold;
    }

    /**
     * @throws InvalidArgumentException
     */
    private function guardConstantSalaryBonus(): void
    {
        if ($this->salaryBonusType === SalaryBonusType::CONSTANT && $this->constantSalaryBonus === null) {
            throw new InvalidArgumentException(sprintf(
                "Constant salary bonus cannot be null with Salary Bonus Type: '%s'",
                SalaryBonusType::CONSTANT->value
            ));
        }
    }

    /**
     * @throws InvalidArgumentException
     */
    private function guardPercentageOfSalaryBonusAndPercentageSalaryBonusThreshold(): void
    {
        if ($this->salaryBonusType === SalaryBonusType::PERCENTAGE
            && (
                $this->percentageOfSalaryBonus === null
                || $this->percentageSalaryBonusThreshold === null
            )
        ) {
            throw new InvalidArgumentException(sprintf(
                "Percentage of  salary bonus and percentage salary bonus threshold cannot be null with Salary Bonus Type: '%s'",
                SalaryBonusType::PERCENTAGE->value
            ));
        }
    }
}
