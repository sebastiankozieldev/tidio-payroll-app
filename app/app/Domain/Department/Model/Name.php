<?php

declare(strict_types=1);

namespace App\Domain\Department\Model;

use InvalidArgumentException;

final class Name
{
    /**
     * @throws InvalidArgumentException
     */
    private function __construct(private string $name)
    {
        if (trim($this->name) === '') {
            throw new InvalidArgumentException('Name cannot be empty');
        }
    }

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @throws InvalidArgumentException
     */
    public static function create(string $name): self
    {
        return new self($name);
    }
}
