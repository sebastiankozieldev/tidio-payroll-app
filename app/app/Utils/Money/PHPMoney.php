<?php

declare(strict_types=1);

namespace App\Utils\Money;

use App\Utils\Money\Exception\{UnableToFormatAmountException, UnableToParseMoneyException};
use Money\Currencies\ISOCurrencies;
use Money\Exception\{FormatterException, ParserException};
use Money\Formatter\DecimalMoneyFormatter;
use Money\Parser\DecimalMoneyParser;
use Money\{Currencies, Currency, Money, MoneyFormatter as PHPMoneyFormatter, MoneyParser as PHPMoneyParser};

final class PHPMoney implements MoneyFormatter, MoneyParser
{
    private PHPMoneyParser $parser;

    private PHPMoneyFormatter $formatter;

    private Currency $currency;

    public function __construct()
    {
        $this->currency = new Currency('USD');
        $currencies = new ISOCurrencies();
        $this->parser = new DecimalMoneyParser($currencies);
        $this->formatter = new DecimalMoneyFormatter($currencies);
    }

    /**
     * @throws UnableToFormatAmountException
     */
    public function toMoney(int $amount): string
    {
        try {
            return $this->formatter->format(new Money($amount, $this->currency));
        } catch (FormatterException) {
            throw UnableToFormatAmountException::create($amount);
        }
    }

    /**
     * @throws UnableToParseMoneyException
     */
    public function toInteger(string $money): int
    {
        try {
            $parsed = $this->parser->parse($money, $this->currency);
        } catch (ParserException) {
            throw UnableToParseMoneyException::create($money);
        }

        return (int) $parsed->getAmount();
    }
}
