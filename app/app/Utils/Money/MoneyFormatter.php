<?php

declare(strict_types=1);

namespace App\Utils\Money;

use App\Utils\Money\Exception\UnableToFormatAmountException;

interface MoneyFormatter
{
    /**
     * @throws UnableToFormatAmountException
     */
    public function toMoney(int $amount): string;
}
