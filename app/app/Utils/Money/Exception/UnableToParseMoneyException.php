<?php

declare(strict_types=1);

namespace App\Utils\Money\Exception;

use Exception;

final class UnableToParseMoneyException extends Exception
{
    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function create(string $money): self
    {
        return new self(sprintf("Unable to parse: '%s'", $money));
    }
}
