<?php

declare(strict_types=1);

namespace App\Utils\Money\Exception;

use Exception;

final class UnableToFormatAmountException extends Exception
{
    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function create(int $amount): self
    {
        return new self(sprintf("Unable to format amount: '%d'", $amount));
    }
}
