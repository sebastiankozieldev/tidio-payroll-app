<?php

declare(strict_types=1);

namespace App\Utils\Money;

use App\Utils\Money\Exception\UnableToParseMoneyException;

interface MoneyParser
{
    /**
     * @throws UnableToParseMoneyException
     */
    public function toInteger(string $money): int;
}
