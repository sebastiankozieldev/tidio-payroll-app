<?php

declare(strict_types=1);

namespace App\Utils\DateTime\Exception;

use Exception;
use Throwable;

final class UnableToCreateDateTimeException extends Exception
{
    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function create(Throwable $previous): self
    {
        return new self(sprintf("Unable to obtain new instance of date time. Reason: '%s", $previous->getMessage()));
    }
}
