<?php

declare(strict_types=1);

namespace App\Utils\DateTime;

use App\Utils\DateTime\Exception\UnableToCreateDateTimeException;
use DateTimeImmutable;

interface DateTimeProvider
{
    /**
     * @throws UnableToCreateDateTimeException
     */
    public function now(): DateTimeImmutable;

    /**
     * @throws UnableToCreateDateTimeException
     */
    public function create(string $date): DateTimeImmutable;
}
