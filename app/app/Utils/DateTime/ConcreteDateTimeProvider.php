<?php

declare(strict_types=1);

namespace App\Utils\DateTime;

use App\Utils\DateTime\Exception\UnableToCreateDateTimeException;
use DateTimeImmutable;
use Exception;

final class ConcreteDateTimeProvider implements DateTimeProvider
{
    /**
     * @throws UnableToCreateDateTimeException
     */
    public function now(): DateTimeImmutable
    {
        return $this->create('now');
    }

    /**
     * @throws UnableToCreateDateTimeException
     */
    public function create(string $date): DateTimeImmutable
    {
        try {
            return new DateTimeImmutable($date);
        } catch (Exception $exception) {
            throw UnableToCreateDateTimeException::create($exception);
        }
    }
}
