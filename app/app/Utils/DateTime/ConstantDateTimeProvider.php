<?php

declare(strict_types=1);

namespace App\Utils\DateTime;

use DateTimeImmutable;

final class ConstantDateTimeProvider implements DateTimeProvider
{
    public const CONSTANT_DATE_TIME = '2022-01-01 00:00:00';

    public function now(): DateTimeImmutable
    {
        return $this->create(self::CONSTANT_DATE_TIME);
    }

    public function create(string $date): DateTimeImmutable
    {
        return new DateTimeImmutable($date);
    }
}
