<?php

declare(strict_types=1);

namespace App\Infrastructure\Laravel\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Infrastructure\Laravel\Models\Department.
 *
 * @property string $id
 * @property string $name
 * @property string $salary_bonus_type
 * @property null|int $constant_salary_bonus
 * @property null|float $percentage_of_salary_bonus
 * @property null|int $percentage_salary_bonus_threshold
 * @property null|\Carbon\CarbonImmutable $created_at
 * @property null|\Carbon\CarbonImmutable $updated_at
 * @property \App\Infrastructure\Laravel\Models\Employee[]|\Illuminate\Database\Eloquent\Collection $employees
 * @property null|int $employees_count
 *
 * @method static \Database\Factories\Infrastructure\Laravel\Models\DepartmentFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Department query()
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereConstantSalaryBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department wherePercentageOfSalaryBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department wherePercentageSalaryBonusThreshold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereSalaryBonusType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Department extends Model
{
    use HasFactory;
    use Uuid;

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'name',
        'salary_bonus_type',
        'constant_salary_bonus',
        'percentage_of_salary_bonus',
        'percentage_salary_bonus_threshold',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'percentage_of_salary_bonus' => 'float',
    ];

    public function employees(): HasMany
    {
        return $this->hasMany(Employee::class);
    }
}
