<?php

declare(strict_types=1);

namespace App\Infrastructure\Laravel\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Infrastructure\Laravel\Models\Employee.
 *
 * @property string $id
 * @property string $first_name
 * @property string $last_name
 * @property int $base_salary
 * @property string $department_id
 * @property \Carbon\CarbonImmutable $employment_started_at
 * @property null|\Carbon\CarbonImmutable $created_at
 * @property null|\Carbon\CarbonImmutable $updated_at
 * @property \App\Infrastructure\Laravel\Models\Department $department
 *
 * @method static \Database\Factories\Infrastructure\Laravel\Models\EmployeeFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Employee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Employee query()
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereBaseSalary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereEmploymentStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Employee extends Model
{
    use HasFactory;
    use Uuid;

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'base_salary',
        'department_id',
        'employment_started_at',
    ];

    /**
     * @var string[]
     */
    protected $dates = ['employment_started_at'];

    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class);
    }
}
