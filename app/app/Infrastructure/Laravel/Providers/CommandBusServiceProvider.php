<?php

declare(strict_types=1);

namespace App\Infrastructure\Laravel\Providers;

use App\Application\CommandBus\{CommandBus, ErrorStorage, InMemoryErrorStorage};
use App\Application\Department\CreateDepartmentWithConstantSalaryBonus\CreateDepartmentWithConstantSalaryBonus;
use App\Application\Department\CreateDepartmentWithConstantSalaryBonus\{CreateDepartmentWithConstantSalaryBonusHandler};
use App\Application\Department\CreateDepartmentWithPercentageSalaryBonus\{CreateDepartmentWithPercentageSalaryBonus, CreateDepartmentWithPercentageSalaryBonusHandler};
use App\Application\Employee\CreateEmployee\{CreateEmployee, CreateEmployeeHandler};
use App\Infrastructure\CommandBus\LaravelCommandBus;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\ServiceProvider;

final class CommandBusServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        parent::register();
        $this->app->singleton(CommandBus::class, LaravelCommandBus::class);
        $this->app->singleton(ErrorStorage::class, InMemoryErrorStorage::class);
    }

    /**
     * @throws BindingResolutionException
     */
    public function boot(): void
    {
        /** @var CommandBus $commandBus */
        $commandBus = $this->app->make(CommandBus::class);
        $commandBus->map([
            CreateDepartmentWithConstantSalaryBonus::class => CreateDepartmentWithConstantSalaryBonusHandler::class,
            CreateDepartmentWithPercentageSalaryBonus::class => CreateDepartmentWithPercentageSalaryBonusHandler::class,
            CreateEmployee::class => CreateEmployeeHandler::class,
        ]);
    }
}
