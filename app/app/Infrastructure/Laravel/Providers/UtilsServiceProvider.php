<?php

declare(strict_types=1);

namespace App\Infrastructure\Laravel\Providers;

use App\Utils\DateTime\{ConcreteDateTimeProvider, DateTimeProvider};
use App\Utils\Money\{MoneyFormatter, MoneyParser, PHPMoney};
use Illuminate\Support\ServiceProvider;

final class UtilsServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        parent::register();
        $this->app->bind(DateTimeProvider::class, ConcreteDateTimeProvider::class);
        $this->app->bind(MoneyFormatter::class, PHPMoney::class);
        $this->app->bind(MoneyParser::class, PHPMoney::class);
    }
}
