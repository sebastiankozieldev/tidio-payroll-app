<?php

declare(strict_types=1);

namespace App\Infrastructure\Laravel\Providers;

use App\Application\Payroll\GetPayroll\GetPayrollQueryHandler;
use App\Infrastructure\Payroll\GetPayroll\EloquentGetPayrollQueryHandler;
use Illuminate\Support\ServiceProvider;

final class QueryHandlerServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        parent::register();
        $this->app->bind(GetPayrollQueryHandler::class, EloquentGetPayrollQueryHandler::class);
    }
}
