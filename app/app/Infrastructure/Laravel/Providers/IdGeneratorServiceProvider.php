<?php

declare(strict_types=1);

namespace App\Infrastructure\Laravel\Providers;

use App\Domain\Common\Id\Department\DepartmentIdGenerator;
use App\Domain\Common\Id\Employee\EmployeeIdGenerator;
use App\Infrastructure\Common\Id\Department\UuidDepartmentIdGenerator;
use App\Infrastructure\Common\Id\Employee\UuidEmployeeIdGenerator;
use Illuminate\Support\ServiceProvider;

final class IdGeneratorServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        parent::register();
        $this->app->bind(DepartmentIdGenerator::class, UuidDepartmentIdGenerator::class);
        $this->app->bind(EmployeeIdGenerator::class, UuidEmployeeIdGenerator::class);
    }
}
