<?php

declare(strict_types=1);

namespace App\Infrastructure\Laravel\Providers;

use App\Application\Department\Repository\DepartmentRepository as ApplicationDepartmentRepository;
use App\Application\Transaction\Transaction;
use App\Domain\Department\Repository\DepartmentRepository as DomainDepartmentRepository;
use App\Domain\Employee\Repository\EmployeeRepository;
use App\Infrastructure\Department\Repository\EloquentDepartmentRepository;
use App\Infrastructure\Employee\Repository\EloquentEmployeeRepository;
use App\Infrastructure\Transaction\LaravelTransaction;
use Illuminate\Support\ServiceProvider;

final class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        parent::register();
        $this->app->bind(Transaction::class, LaravelTransaction::class);
        $this->registerDomainRepositories();
        $this->registerApplicationRepositories();
    }

    private function registerDomainRepositories(): void
    {
        $this->app->bind(DomainDepartmentRepository::class, EloquentDepartmentRepository::class);
        $this->app->bind(EmployeeRepository::class, EloquentEmployeeRepository::class);
    }

    private function registerApplicationRepositories(): void
    {
        $this->app->bind(ApplicationDepartmentRepository::class, EloquentDepartmentRepository::class);
    }
}
