<?php

declare(strict_types=1);

namespace App\Infrastructure\Laravel\Http\Controllers;

use App\Application\Payroll\GetPayroll\GetPayrollQueryHandler;
use App\Infrastructure\Laravel\Http\Requests\GetPayrollRequest;
use App\Infrastructure\Laravel\Http\Resources\EmployeeResource;
use App\Infrastructure\Laravel\Http\Transformers\GetPayrollQueryTransformer;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PayrollController extends Controller
{
    public function __construct(
        private GetPayrollQueryHandler $getPayrollQueryHandler,
        private GetPayrollQueryTransformer $getPayrollQueryTransformer
    ) {
    }

    public function index(GetPayrollRequest $getPayrollRequest): AnonymousResourceCollection
    {
        $query = $this->getPayrollQueryTransformer->requestToQuery($getPayrollRequest);

        $payroll = $this->getPayrollQueryHandler->get($query);

        return EmployeeResource::collection($payroll->getEmployees());
    }
}
