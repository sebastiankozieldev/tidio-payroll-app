<?php

declare(strict_types=1);

namespace App\Infrastructure\Laravel\Http\Resources;

use App\Application\Payroll\Model\{Employee, Payroll};
use App\Utils\Money\MoneyFormatter;
use Illuminate\Http\Resources\Json\JsonResource;

final class EmployeeResource extends JsonResource
{
    /**
     * @param mixed $request
     *
     * @return mixed[]
     */
    public function toArray($request): array
    {
        /** @var Employee $employee */
        $employee = $this->resource;

        /** @var MoneyFormatter $moneyFormatter */
        $moneyFormatter = app(MoneyFormatter::class);

        $salary = $employee->getSalary();

        return [
            'first_name' => (string) $employee->getFirstName(),
            'last_name' => (string) $employee->getLastName(),
            'department_name' => (string) $employee->getName(),
            'base_salary' => $moneyFormatter->toMoney($salary->getBaseSalary()->getAmount()),
            'salary_bonus' => $moneyFormatter->toMoney($salary->getSalaryBonus()->getAmount()),
            'salary_bonus_type' => $salary->getSalaryBonusType()->value,
            'total_salary' => $moneyFormatter->toMoney($salary->getAmount()),
        ];
    }
}
