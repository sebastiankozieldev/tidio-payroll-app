<?php

declare(strict_types=1);

namespace App\Infrastructure\Laravel\Http\Transformers;

use App\Application\Payroll\GetPayroll\{GetPayrollQuery, PayrollFilter, PayrollOrderBy};
use App\Application\Query\{Filter, OrderDirection};
use App\Infrastructure\Laravel\Http\Requests\GetPayrollRequest;

class GetPayrollQueryTransformer
{
    public function requestToQuery(GetPayrollRequest $request): GetPayrollQuery
    {
        $payrollOrderBy = $request->validated('order_by', PayrollOrderBy::LAST_NAME->value);

        $orderDirection = $request->validated('order_direction', OrderDirection::ASC->value);

        $filters = $this->transformFilters($request);

        return GetPayrollQuery::create(
            PayrollOrderBy::from($payrollOrderBy),
            OrderDirection::from($orderDirection),
            ...$filters
        );
    }

    /**
     * @return Filter[]
     */
    private function transformFilters(GetPayrollRequest $request): array
    {
        $filters = [];

        $departmentName = $request->get('department_name');

        if ($departmentName !== null) {
            $filters[] = Filter::create(PayrollFilter::DEPARTMENT_NAME->value, (string) $departmentName);
        }

        $firstName = $request->get('first_name');

        if ($firstName !== null) {
            $filters[] = Filter::create(PayrollFilter::FIRST_NAME->value, (string) $firstName);
        }

        $lastName = $request->get('last_name');

        if ($lastName !== null) {
            $filters[] = Filter::create(PayrollFilter::LAST_NAME->value, (string) $lastName);
        }

        return $filters;
    }
}
