<?php

declare(strict_types=1);

namespace App\Infrastructure\Laravel\Http\Requests;

use App\Application\Payroll\GetPayroll\PayrollOrderBy;
use App\Application\Query\OrderDirection;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

final class GetPayrollRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            'order_by' => [
                'nullable',
                Rule::in(
                    collect(PayrollOrderBy::cases())->map(
                        fn (PayrollOrderBy $payrollOrderBy) => $payrollOrderBy->value
                    )->all()
                ),
            ],
            'order_direction' => [
                'nullable',
                Rule::in(
                    collect(OrderDirection::cases())->map(
                        fn (OrderDirection $orderDirection) => $orderDirection->value
                    )->all()
                ),
            ],
            'department_name' => ['nullable', 'string'],
            'first_name' => ['nullable', 'string'],
            'last_name' => ['nullable', 'string'],
        ];
    }
}
