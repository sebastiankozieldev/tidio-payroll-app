<?php

declare(strict_types=1);

namespace App\Infrastructure\Laravel\Console\Commands;

use App\Application\CommandBus\{CommandBus, ErrorStorage};
use App\Application\Department\Model\Department;
use App\Application\Department\Repository\DepartmentRepository;
use App\Application\Employee\CreateEmployee\CreateEmployee;
use App\Domain\Common\Id\Department\DepartmentId;
use App\Domain\Common\Model\BaseSalary;
use App\Domain\Employee\Model\{FirstName, LastName};
use App\Utils\Money\Exception\UnableToParseMoneyException;
use App\Utils\Money\MoneyParser;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;

final class CreateEmployeeCommand extends Command
{
    protected $signature = 'employee:create';

    protected $description = 'Create new employee';

    /**
     * @var Department[]
     */
    private array $departments;

    public function __construct(
        private CommandBus $commandBus,
        private ErrorStorage $errorStorage,
        private DepartmentRepository $departmentRepository,
        private MoneyParser $moneyParser
    ) {
        parent::__construct();
    }

    public function handle(): void
    {
        $departmentsChoice = $this->prepareDepartmentsChoice();

        if ($departmentsChoice === []) {
            $this->error('It seems there are not departments. Create some first');

            return;
        }

        $firstName = trim((string) $this->ask('First name?'));

        if ($firstName === '') {
            $this->error('First name cannot be empty');

            return;
        }

        $lastName = trim((string) $this->ask('Last name?'));

        if ($lastName === '') {
            $this->error('Last name cannot be empty');

            return;
        }

        try {
            $baseSalary = $this->moneyParser->toInteger($this->ask('Base salary? e.g. 2000.00'));
        } catch (UnableToParseMoneyException $exception) {
            $this->error($exception->getMessage());

            return;
        }

        if ($baseSalary < 0) {
            $this->error('Base salary cannot be negative');

            return;
        }

        $employmentStartDate = trim((string) $this->ask('Employment start date? (dd-mm-yyyy)'));

        $employmentStartDate = CarbonImmutable::createFromFormat('d-m-Y', $employmentStartDate);

        if ($employmentStartDate === false) {
            $this->error('Provided employment start date is not valid');

            return;
        }

        /** @phpstan-ignore-next-line $this->choice returns string|array, error:  Cannot cast array|string to string, array is returned only with multiple choices */
        $departmentName = (string) $this->choice('Department?', $departmentsChoice);

        $departmentId = $this->findDepartmentIdForName($departmentName);

        $command = CreateEmployee::create(
            FirstName::create($firstName),
            LastName::create($lastName),
            BaseSalary::create($baseSalary),
            $employmentStartDate->toDateTimeImmutable(),
            $departmentId
        );

        $this->commandBus->dispatch($command);

        $errors = $this->errorStorage->getErrors();
        $this->errorStorage->clear();

        if ($errors === []) {
            $this->info('Employee has been created');

            return;
        }

        $this->error('Unable to create employee');
        foreach ($errors as $error) {
            $this->error($error);
        }
    }

    /**
     * @return string[]
     */
    private function prepareDepartmentsChoice(): array
    {
        $this->departments = $this->departmentRepository->findAll();

        return collect($this->departments)->map(
            fn (Department $department): string => (string) $department->getName()
        )->all();
    }

    private function findDepartmentIdForName(string $name): DepartmentId
    {
        /** @var Department $department */
        $department = collect($this->departments)->first(
            fn (Department $department): bool => (string) $department->getName() === $name
        );

        return $department->getDepartmentId();
    }
}
