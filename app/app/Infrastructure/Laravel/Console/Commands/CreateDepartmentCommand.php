<?php

declare(strict_types=1);

namespace App\Infrastructure\Laravel\Console\Commands;

use App\Application\CommandBus\{CommandBus, ErrorStorage};
use App\Application\Department\CreateDepartmentWithConstantSalaryBonus\CreateDepartmentWithConstantSalaryBonus;
use App\Application\Department\CreateDepartmentWithPercentageSalaryBonus\CreateDepartmentWithPercentageSalaryBonus;
use App\Domain\Common\Model\{ConstantSalaryBonus, PercentageOfSalaryBonus, PercentageSalaryBonusThreshold};
use App\Domain\Department\Model\{Name, SalaryBonusType};
use App\Utils\Money\Exception\UnableToParseMoneyException;
use App\Utils\Money\MoneyParser;
use Illuminate\Console\Command;

final class CreateDepartmentCommand extends Command
{
    protected $signature = 'department:create';

    protected $description = 'Create new department';

    public function __construct(
        private CommandBus $commandBus,
        private ErrorStorage $errorStorage,
        private MoneyParser $moneyParser
    ) {
        parent::__construct();
    }

    public function handle(): void
    {
        $departmentName = trim((string) $this->ask('Department name? e.g. HR'));

        if ($departmentName === '') {
            $this->error('Department name cannot be empty');

            return;
        }

        $departmentName = Name::create($departmentName);

        /** @phpstan-ignore-next-line $this->choice returns string|array, error:  Cannot cast array|string to string, array is returned only with multiple choices*/
        $salaryBonusType = (string) $this->choice(
            'Salary bonus type?',
            [SalaryBonusType::CONSTANT->value, SalaryBonusType::PERCENTAGE->value]
        );

        $salaryBonusType = SalaryBonusType::from($salaryBonusType);

        if ($salaryBonusType === SalaryBonusType::CONSTANT) {
            try {
                $bonus = $this->moneyParser->toInteger($this->ask('Salary bonus? e.g. 199.99'));
            } catch (UnableToParseMoneyException $exception) {
                $this->error($exception->getMessage());

                return;
            }

            if ($bonus < 0) {
                $this->error('Salary bonus cannot be negative');

                return;
            }
            $constantSalaryBonus = ConstantSalaryBonus::create($bonus);

            $command = CreateDepartmentWithConstantSalaryBonus::create($departmentName, $constantSalaryBonus);
        } else {
            $percentage = (float) $this->ask('Percentage of salary bonus? e.g. 5.5');
            if ($percentage < 0) {
                $this->error('Percentage cannot be negative');

                return;
            }
            $percentageOfSalaryBonus = PercentageOfSalaryBonus::create($percentage);

            $years = (int) $this->ask('Required years of service to receive salary bonus? e.g. 10');
            if ($years < 0) {
                $this->error('Required years cannot be negative');

                return;
            }

            $percentageSalaryBonusThreshold = PercentageSalaryBonusThreshold::create($years);

            $command = CreateDepartmentWithPercentageSalaryBonus::create(
                $departmentName,
                $percentageOfSalaryBonus,
                $percentageSalaryBonusThreshold
            );
        }

        $this->commandBus->dispatch($command);

        $errors = $this->errorStorage->getErrors();
        $this->errorStorage->clear();

        if ($errors === []) {
            $this->info('Department has been created');

            return;
        }

        $this->error('Unable to create department');
        foreach ($errors as $error) {
            $this->error($error);
        }
    }
}
