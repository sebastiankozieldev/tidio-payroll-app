<?php

declare(strict_types=1);

namespace App\Infrastructure\Transaction;

use App\Application\Transaction\Transaction;
use Illuminate\Database\ConnectionInterface;

final class LaravelTransaction implements Transaction
{
    public function __construct(private ConnectionInterface $connection)
    {
    }

    public function begin(): void
    {
        $this->connection->beginTransaction();
    }

    public function rollBack(): void
    {
        $this->connection->rollBack();
    }

    public function commit(): void
    {
        $this->connection->commit();
    }
}
