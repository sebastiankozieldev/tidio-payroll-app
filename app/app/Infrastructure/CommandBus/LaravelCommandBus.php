<?php

declare(strict_types=1);

namespace App\Infrastructure\CommandBus;

use App\Application\CommandBus\{Command, CommandBus, Exception\MissingHandlerException};
use Illuminate\Contracts\Bus\Dispatcher;

final class LaravelCommandBus implements CommandBus
{
    /**
     * @var string[]
     */
    private array $map = [];

    public function __construct(private Dispatcher $dispatcher)
    {
    }

    /**
     * @throws MissingHandlerException
     */
    public function dispatch(Command $command): void
    {
        if (\array_key_exists($command::class, $this->map) === false) {
            throw MissingHandlerException::create($command);
        }
        $this->dispatcher->dispatchSync($command);
    }

    public function map(array $map): void
    {
        $this->map = $map;
        $this->dispatcher->map($map);
    }
}
