<?php

declare(strict_types=1);

namespace App\Infrastructure\Payroll\GetPayroll;

use App\Application\Payroll\GetPayroll\{GetPayrollQuery, GetPayrollQueryHandler, PayrollFilter, PayrollOrderBy};
use App\Application\Payroll\Model\{Employee, Payroll};
use App\Application\Query\{Filter, OrderDirection};
use App\Infrastructure\Laravel\Models\Employee as EloquentEmployee;
use App\Infrastructure\Payroll\GetPayroll\Transformer\EmployeeTransformer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use RuntimeException;

final class EloquentGetPayrollQueryHandler implements GetPayrollQueryHandler
{
    public function __construct(private EmployeeTransformer $employeeTransformer)
    {
    }

    public function get(GetPayrollQuery $getPayrollQuery): Payroll
    {
        $orderDirection = $getPayrollQuery->getOrderDirection();
        $payrollOrderBy = $getPayrollQuery->getPayrollOrderBy();

        $query = EloquentEmployee::query();

        /** @phpstan-ignore-next-line ignoring Dynamic call to static method ::join() */
        $query->join('departments', 'employees.department_id', '=', 'departments.id')
            ->with('department');

        self::applyFilters($query, ...$getPayrollQuery->getFilters());

        if ($this->shouldSortOnQuery($payrollOrderBy) === true) {
            /** @phpstan-ignore-next-line ignoring Dynamic call to static method ::orderBy() */
            $query->orderBy(self::mapPayrollOrderByToColumn($payrollOrderBy), $orderDirection->value);
        }

        $employees = $query->get()->map(
            fn (EloquentEmployee $employee): Employee => $this->employeeTransformer->eloquentToApplication(
                $employee
            )
        );

        if ($this->shouldSortResults($payrollOrderBy) === true) {
            $employees = $this->sortResults($employees, $payrollOrderBy, $orderDirection);
        }

        return Payroll::create(...$employees->all());
    }

    private function shouldSortOnQuery(PayrollOrderBy $payrollOrderBy): bool
    {
        return $payrollOrderBy !== PayrollOrderBy::SALARY_BONUS && $payrollOrderBy !== PayrollOrderBy::TOTAL_SALARY;
    }

    private function shouldSortResults(PayrollOrderBy $payrollOrderBy): bool
    {
        return $payrollOrderBy === PayrollOrderBy::SALARY_BONUS || $payrollOrderBy === PayrollOrderBy::TOTAL_SALARY;
    }

    /**
     * @param Collection|Employee[] $employees
     *
     * @throws RuntimeException
     *
     * @return Collection|Employee[]
     */
    private function sortResults(
        Collection $employees,
        PayrollOrderBy $payrollOrderBy,
        OrderDirection $orderDirection
    ): Collection {
        return $employees->sortBy(function (Employee $employee) use ($payrollOrderBy): int {
            $salary = $employee->getSalary();

            return match ($payrollOrderBy) {
                PayrollOrderBy::SALARY_BONUS => $salary->getSalaryBonus()->getAmount(),
                PayrollOrderBy::TOTAL_SALARY => $salary->getAmount(),
                default => throw new RuntimeException(sprintf(
                    "Results sorting is not available for PayrollOrderBy: '%s'",
                    $payrollOrderBy->value
                ))
            };
        }, \SORT_REGULAR, $orderDirection === OrderDirection::DESC);
    }

    /**
     * @throws RuntimeException
     */
    private static function mapPayrollOrderByToColumn(PayrollOrderBy $payrollOrderBy): string
    {
        $exceptionMessage = sprintf(
            "Sorting on query is not available for PayrollOrderBy: '%s'",
            $payrollOrderBy->value
        );

        return match ($payrollOrderBy) {
            PayrollOrderBy::FIRST_NAME => 'first_name',
            PayrollOrderBy::LAST_NAME => 'last_name',
            PayrollOrderBy::DEPARTMENT_NAME => 'departments.name',
            PayrollOrderBy::BASE_SALARY => 'base_salary',
            PayrollOrderBy::SALARY_BONUS_TYPE => 'departments.salary_bonus_type',
            PayrollOrderBy::SALARY_BONUS => throw new RuntimeException($exceptionMessage),
            PayrollOrderBy::TOTAL_SALARY => throw new RuntimeException($exceptionMessage)
        };
    }

    private static function mapPayrollFilterToColumn(PayrollFilter $payrollFilter): string
    {
        return match ($payrollFilter) {
            PayrollFilter::DEPARTMENT_NAME => 'departments.name',
            PayrollFilter::FIRST_NAME => 'first_name',
            PayrollFilter::LAST_NAME => 'last_name',
        };
    }

    private static function applyFilters(Builder $query, Filter ...$filters): void
    {
        foreach ($filters as $filter) {
            $query->where(
                self::mapPayrollFilterToColumn(PayrollFilter::from($filter->getField())),
                '=',
                $filter->getValue()
            );
        }
    }
}
