<?php

declare(strict_types=1);

namespace App\Infrastructure\Payroll\GetPayroll\Transformer;

use App\Application\Payroll\Model\Employee as ApplicationEmployee;
use App\Domain\Common\Model\{BaseSalary, ConstantSalaryBonus, PercentageOfSalaryBonus, PercentageSalaryBonusThreshold};
use App\Domain\Department\Model\{Name, SalaryBonusType};
use App\Domain\Employee\Model\{FirstName, LastName};
use App\Domain\Payroll\Factory\SalaryFactory;
use App\Domain\Payroll\Model\Salary;
use App\Infrastructure\Laravel\Models\{Department, Employee as EloquentEmployee};

class EmployeeTransformer
{
    public function __construct(private SalaryFactory $salaryFactory)
    {
    }

    public function eloquentToApplication(EloquentEmployee $employee): ApplicationEmployee
    {
        $department = $employee->department;

        return ApplicationEmployee::create(
            FirstName::create($employee->first_name),
            LastName::create($employee->last_name),
            Name::create($department->name),
            $this->transformSalary($employee, $department)
        );
    }

    private function transformSalary(EloquentEmployee $employee, Department $department): Salary
    {
        $baseSalary = BaseSalary::create($employee->base_salary);

        $employmentStartDate = $employee->employment_started_at->toDateTimeImmutable();

        $salaryBonusType = SalaryBonusType::from($department->salary_bonus_type);

        return match ($salaryBonusType) {
            SalaryBonusType::CONSTANT => $this->salaryFactory->makeSalaryWithConstantSalaryBonus(
                $baseSalary,
                $employmentStartDate,
                ConstantSalaryBonus::create((int) $department->constant_salary_bonus)
            ),
            SalaryBonusType::PERCENTAGE => $this->salaryFactory->makeSalaryWithPercentageSalaryBonus(
                $baseSalary,
                $employmentStartDate,
                PercentageOfSalaryBonus::create((float) $department->percentage_of_salary_bonus),
                PercentageSalaryBonusThreshold::create((int) $department->percentage_salary_bonus_threshold)
            )
        };
    }
}
