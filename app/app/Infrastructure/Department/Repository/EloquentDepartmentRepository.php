<?php

declare(strict_types=1);

namespace App\Infrastructure\Department\Repository;

use App\Application\Department\Model\Department as ApplicationDepartment;
use App\Application\Department\Repository\DepartmentRepository as ApplicationDepartmentRepository;
use App\Domain\Department\Model\{Department, Name};
use App\Domain\Department\Repository\DepartmentRepository as DomainDepartmentRepository;
use App\Domain\Department\Repository\Exception\UnableToSaveDepartmentException;
use App\Infrastructure\Department\Repository\Transformer\DepartmentTransformer;
use App\Infrastructure\Laravel\Models\Department as EloquentDepartment;
use Throwable;

final class EloquentDepartmentRepository implements ApplicationDepartmentRepository, DomainDepartmentRepository
{
    public function __construct(private DepartmentTransformer $departmentTransformer)
    {
    }

    /**
     * @throws UnableToSaveDepartmentException
     */
    public function save(Department $department): void
    {
        $eloquentDepartment = $this->departmentTransformer->domainToEloquent($department);

        try {
            $eloquentDepartment->saveOrFail();
        } catch (Throwable $exception) {
            throw UnableToSaveDepartmentException::create($department->getDepartmentId(), $exception);
        }
    }

    public function existsWithName(Name $departmentName): bool
    {
        /** @phpstan-ignore-next-line */
        return EloquentDepartment::whereName((string) $departmentName)->exists();
    }

    /**
     * @return ApplicationDepartment[]
     */
    public function findAll(): array
    {
        return EloquentDepartment::all()->map(
            fn (EloquentDepartment $department): ApplicationDepartment => $this->departmentTransformer->eloquentToApplication(
                $department
            )
        )->all();
    }
}
