<?php

declare(strict_types=1);

namespace App\Infrastructure\Department\Repository\Transformer;

use App\Application\Department\Model\Department as ApplicationDepartment;
use App\Domain\Department\Model\{Department as DomainDepartment, Name};
use App\Infrastructure\Common\Id\Department\UuidDepartmentId;
use App\Infrastructure\Laravel\Models\Department as EloquentDepartment;

class DepartmentTransformer
{
    public function domainToEloquent(DomainDepartment $department): EloquentDepartment
    {
        return new EloquentDepartment([
            'id' => (string) $department->getDepartmentId(),
            'name' => (string) $department->getName(),
            'salary_bonus_type' => $department->getSalaryBonusType()->value,
            'constant_salary_bonus' => $department->getConstantSalaryBonus()?->getAmount(),
            'percentage_of_salary_bonus' => $department->getPercentageOfSalaryBonus()?->getPercentage(),
            'percentage_salary_bonus_threshold' => $department->getPercentageSalaryBonusThreshold()?->getYears(),
        ]);
    }

    public function eloquentToApplication(EloquentDepartment $department): ApplicationDepartment
    {
        return ApplicationDepartment::create(
            UuidDepartmentId::from($department->id),
            Name::create($department->name)
        );
    }
}
