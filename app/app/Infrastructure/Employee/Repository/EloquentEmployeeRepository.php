<?php

declare(strict_types=1);

namespace App\Infrastructure\Employee\Repository;

use App\Domain\Employee\Model\Employee;
use App\Domain\Employee\Repository\EmployeeRepository;
use App\Domain\Employee\Repository\Exception\UnableToSaveEmployeeException;
use App\Infrastructure\Employee\Repository\Transformer\EmployeeTransformer;
use Throwable;

final class EloquentEmployeeRepository implements EmployeeRepository
{
    public function __construct(private EmployeeTransformer $employeeTransformer)
    {
    }

    public function save(Employee $employee): void
    {
        $eloquentEmployee = $this->employeeTransformer->domainToEloquent($employee);

        try {
            $eloquentEmployee->saveOrFail();
        } catch (Throwable $exception) {
            throw UnableToSaveEmployeeException::create($employee->getEmployeeId(), $exception);
        }
    }
}
