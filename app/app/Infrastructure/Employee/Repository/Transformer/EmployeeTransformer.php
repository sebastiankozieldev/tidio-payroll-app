<?php

declare(strict_types=1);

namespace App\Infrastructure\Employee\Repository\Transformer;

use App\Domain\Employee\Model\Employee;
use App\Infrastructure\Laravel\Models\Employee as EloquentEmployee;

class EmployeeTransformer
{
    public function domainToEloquent(Employee $employee): EloquentEmployee
    {
        return new EloquentEmployee([
            'id' => (string) $employee->getEmployeeId(),
            'first_name' => (string) $employee->getFirstName(),
            'last_name' => (string) $employee->getLastName(),
            'base_salary' => $employee->getBaseSalary()->getAmount(),
            'department_id' => (string) $employee->getDepartmentId(),
            'employment_started_at' => $employee->getEmploymentStartDate(),
        ]);
    }
}
