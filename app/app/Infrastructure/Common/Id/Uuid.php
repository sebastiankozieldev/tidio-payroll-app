<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Id;

use InvalidArgumentException;
use Ramsey\Uuid\Uuid as RamseyUuid;

abstract class Uuid
{
    protected function __construct(private string $id)
    {
        if (RamseyUuid::isValid($id) === false) {
            throw new InvalidArgumentException(sprintf("'%s' is not valid uuid", $id));
        }
    }

    public function __toString(): string
    {
        return $this->id;
    }

    protected static function generate(): string
    {
        return (string) RamseyUuid::uuid4();
    }
}
