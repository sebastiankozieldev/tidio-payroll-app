<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Id\Department;

use App\Domain\Common\Id\Department\{DepartmentId, DepartmentIdGenerator};

final class UuidDepartmentIdGenerator implements DepartmentIdGenerator
{
    public function generate(): DepartmentId
    {
        return UuidDepartmentId::create();
    }
}
