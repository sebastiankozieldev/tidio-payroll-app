<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Id\Department;

use App\Domain\Common\Id\Department\DepartmentId;
use App\Infrastructure\Common\Id\Uuid;

final class UuidDepartmentId extends Uuid implements DepartmentId
{
    public static function create(): DepartmentId
    {
        return new self(self::generate());
    }

    public static function from(string $id): DepartmentId
    {
        return new self($id);
    }
}
