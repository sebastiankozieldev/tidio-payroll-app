<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Id\Employee;

use App\Domain\Common\Id\Employee\EmployeeId;
use App\Infrastructure\Common\Id\Uuid;

final class UuidEmployeeId extends Uuid implements EmployeeId
{
    public static function from(string $id): EmployeeId
    {
        return new self($id);
    }

    public static function create(): EmployeeId
    {
        return new self(self::generate());
    }
}
