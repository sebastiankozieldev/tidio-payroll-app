<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Id\Employee;

use App\Domain\Common\Id\Employee\{EmployeeId, EmployeeIdGenerator};

final class UuidEmployeeIdGenerator implements EmployeeIdGenerator
{
    public function generate(): EmployeeId
    {
        return UuidEmployeeId::create();
    }
}
