<?php

declare(strict_types=1);

namespace Tests\Unit\Application\Employee\CreateEmployee;

use App\Application\CommandBus\ErrorStorage;
use App\Application\Employee\CreateEmployee\{CreateEmployee, CreateEmployeeHandler};
use App\Application\Transaction\Transaction;
use App\Domain\Common\Id\Employee\EmployeeIdGenerator;
use App\Domain\Common\Model\BaseSalary;
use App\Domain\Employee\Model\{FirstName, LastName};
use App\Domain\Employee\Repository\EmployeeRepository;
use App\Domain\Employee\Repository\Exception\UnableToSaveEmployeeException;
use App\Infrastructure\Common\Id\Department\UuidDepartmentId;
use App\Infrastructure\Common\Id\Employee\UuidEmployeeId;
use PHPUnit\Framework\MockObject\{MockObject, Stub};
use Tests\UnitTestCase;
use Throwable;

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
final class CreateEmployeeHandlerTest extends UnitTestCase
{
    private CreateEmployeeHandler $createEmployeeHandler;

    private EmployeeRepository|MockObject $employeeRepository;

    private EmployeeIdGenerator|Stub $employeeIdGenerator;

    private Transaction|MockObject $transaction;

    private ErrorStorage|MockObject $errorStorage;

    protected function setUp(): void
    {
        parent::setUp();
        $this->employeeRepository = $this->createMock(EmployeeRepository::class);
        $this->employeeIdGenerator = $this->createStub(EmployeeIdGenerator::class);
        $this->transaction = $this->createMock(Transaction::class);
        $this->errorStorage = $this->createMock(ErrorStorage::class);

        $this->createEmployeeHandler = new CreateEmployeeHandler(
            $this->employeeRepository,
            $this->employeeIdGenerator,
            $this->transaction,
            $this->errorStorage
        );
    }

    public function testCreatesEmployee(): void
    {
        //Given
        $command = CreateEmployee::create(
            FirstName::create('first_name'),
            LastName::create('last_name'),
            BaseSalary::create(100000),
            self::createDateTime()->toDateTimeImmutable(),
            UuidDepartmentId::create(),
        );

        $this->employeeIdGenerator->method('generate')
            ->willReturn(UuidEmployeeId::create());

        $this->transaction->expects(self::once())
            ->method('begin');

        $this->employeeRepository->expects(self::once())
            ->method('save');

        $this->transaction->expects(self::once())
            ->method('commit');

        //When
        ($this->createEmployeeHandler)($command);
    }

    public function testAddsErrorWhenUnableToSaveEmployee(): void
    {
        //Given
        $employeeId = UuidEmployeeId::create();

        $command = CreateEmployee::create(
            FirstName::create('first_name'),
            LastName::create('last_name'),
            BaseSalary::create(100000),
            self::createDateTime()->toDateTimeImmutable(),
            UuidDepartmentId::create(),
        );

        $this->employeeIdGenerator->method('generate')
            ->willReturn($employeeId);

        $this->transaction->expects(self::once())
            ->method('begin');

        $this->employeeRepository->expects(self::once())
            ->method('save')
            ->willThrowException(
                UnableToSaveEmployeeException::create($employeeId, $this->createStub(Throwable::class))
            );

        $this->errorStorage->expects(self::once())
            ->method('addError');

        $this->transaction->expects(self::once())
            ->method('rollback');

        //When
        ($this->createEmployeeHandler)($command);
    }
}
