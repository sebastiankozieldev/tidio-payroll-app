<?php

declare(strict_types=1);

namespace Tests\Unit\Application\Department\CreateDepartmentWithPercentageSalaryBonus;

use App\Application\CommandBus\ErrorStorage;
use App\Application\Department\CreateDepartmentWithPercentageSalaryBonus\{CreateDepartmentWithPercentageSalaryBonus, CreateDepartmentWithPercentageSalaryBonusHandler};
use App\Application\Transaction\Transaction;
use App\Domain\Common\Id\Department\DepartmentId;
use App\Domain\Common\Id\Department\{DepartmentIdGenerator};
use App\Domain\Common\Model\{PercentageOfSalaryBonus, PercentageSalaryBonusThreshold};
use App\Domain\Department\Model\Name;
use App\Domain\Department\Repository\DepartmentRepository;
use App\Domain\Department\Repository\Exception\UnableToSaveDepartmentException;
use PHPUnit\Framework\MockObject\{MockObject, Stub};
use Tests\UnitTestCase;
use Throwable;

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
final class CreateDepartmentWithPercentageSalaryBonusHandlerTest extends UnitTestCase
{
    private CreateDepartmentWithPercentageSalaryBonusHandler $createDepartmentWithPercentageSalaryBonusHandler;

    private DepartmentRepository|MockObject $departmentRepository;

    private DepartmentIdGenerator|Stub $departmentIdGenerator;

    private DepartmentRepository|MockObject $transaction;

    private ErrorStorage|MockObject $errorStorage;

    protected function setUp(): void
    {
        parent::setUp();
        $this->departmentRepository = $this->createMock(DepartmentRepository::class);
        $this->departmentIdGenerator = $this->createStub(DepartmentIdGenerator::class);
        $this->transaction = $this->createMock(Transaction::class);
        $this->errorStorage = $this->createMock(ErrorStorage::class);

        $this->createDepartmentWithPercentageSalaryBonusHandler = new CreateDepartmentWithPercentageSalaryBonusHandler(
            $this->departmentRepository,
            $this->departmentIdGenerator,
            $this->transaction,
            $this->errorStorage
        );
    }

    public function testSetsErrorWhenDepartmentNameIsTaken(): void
    {
        //Given
        $departmentName = Name::create('department_name');

        $command = CreateDepartmentWithPercentageSalaryBonus::create(
            $departmentName,
            PercentageOfSalaryBonus::create(0),
            PercentageSalaryBonusThreshold::create(0)
        );

        $this->departmentRepository->expects(self::once())
            ->method('existsWithName')
            ->with($departmentName)
            ->willReturn(true);

        $this->errorStorage->expects(self::once())
            ->method('addError')
            ->with("Department name: 'department_name' is taken. Try another");

        //When
        ($this->createDepartmentWithPercentageSalaryBonusHandler)($command);
    }

    public function testsSetsErrorWhenUnableToSaveDepartment(): void
    {
        //Given
        $departmentName = Name::create('department_name');

        $departmentId = $this->createStub(DepartmentId::class);

        $command = CreateDepartmentWithPercentageSalaryBonus::create(
            $departmentName,
            PercentageOfSalaryBonus::create(0),
            PercentageSalaryBonusThreshold::create(0)
        );

        $this->departmentRepository->expects(self::once())
            ->method('existsWithName')
            ->with($departmentName)
            ->willReturn(false);

        $this->departmentIdGenerator->method('generate')
            ->willReturn($departmentId);

        $this->transaction->expects(self::once())
            ->method('begin');

        $this->departmentRepository->expects(self::once())
            ->method('save')
            ->willThrowException(
                UnableToSaveDepartmentException::create($departmentId, $this->createStub(Throwable::class))
            );

        $this->errorStorage->expects(self::once())
            ->method('addError');

        $this->transaction->expects(self::once())
            ->method('rollBack');

        $this->transaction->expects(self::never())
            ->method('commit');

        //When
        ($this->createDepartmentWithPercentageSalaryBonusHandler)($command);
    }

    public function testHandlesDepartmentCreationSuccessfully(): void
    {
        //Given
        $departmentName = Name::create('department_name');

        $departmentId = $this->createStub(DepartmentId::class);

        $command = CreateDepartmentWithPercentageSalaryBonus::create(
            $departmentName,
            PercentageOfSalaryBonus::create(0),
            PercentageSalaryBonusThreshold::create(0)
        );

        $this->departmentRepository->expects(self::once())
            ->method('existsWithName')
            ->with($departmentName)
            ->willReturn(false);

        $this->departmentIdGenerator->method('generate')
            ->willReturn($departmentId);

        $this->transaction->expects(self::once())
            ->method('begin');

        $this->departmentRepository->expects(self::once())
            ->method('save');

        $this->errorStorage->expects(self::never())
            ->method('addError');

        $this->transaction->expects(self::once())
            ->method('commit');

        //When
        ($this->createDepartmentWithPercentageSalaryBonusHandler)($command);
    }
}
