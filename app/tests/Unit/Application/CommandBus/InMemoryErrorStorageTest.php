<?php

declare(strict_types=1);

namespace Tests\Unit\Application\CommandBus;

use App\Application\CommandBus\InMemoryErrorStorage;
use Tests\UnitTestCase;

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
final class InMemoryErrorStorageTest extends UnitTestCase
{
    private InMemoryErrorStorage $inMemoryErrorStorage;

    protected function setUp(): void
    {
        parent::setUp();
        $this->inMemoryErrorStorage = new InMemoryErrorStorage();
    }

    public function testReturnsEmptyErrorsAfterInitialisation(): void
    {
        //When
        $errors = $this->inMemoryErrorStorage->getErrors();

        //Then
        self::assertEmpty($errors);
    }

    public function testSetsErrors(): void
    {
        //Given
        $errors = ['error_0', 'error_1'];

        //When
        $this->inMemoryErrorStorage->setErrors(...$errors);

        //Then
        $returnedErrors = $this->inMemoryErrorStorage->getErrors();
        self::assertSame($errors, $returnedErrors);
    }

    public function testAddsError(): void
    {
        //Given
        $error = 'error';

        //When
        $this->inMemoryErrorStorage->addError($error);

        //Then
        $errors = $this->inMemoryErrorStorage->getErrors();
        self::assertSame([$error], $errors);
    }

    public function testClearsErrors(): void
    {
        //Given
        $errors = ['error'];
        $this->inMemoryErrorStorage->setErrors(...$errors);

        //When
        $this->inMemoryErrorStorage->clear();

        //Then
        self::assertEmpty($this->inMemoryErrorStorage->getErrors());
    }
}
