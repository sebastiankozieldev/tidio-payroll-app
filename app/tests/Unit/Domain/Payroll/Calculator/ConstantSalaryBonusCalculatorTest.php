<?php

declare(strict_types=1);

namespace Tests\Unit\Domain\Payroll\Calculator;

use App\Domain\Common\Model\{ConstantSalaryBonus, YearsOfService};
use App\Domain\Payroll\Calculator\ConstantSalaryBonusCalculator;
use Tests\UnitTestCase;

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
final class ConstantSalaryBonusCalculatorTest extends UnitTestCase
{
    /**
     * @dataProvider inputDataProvider
     */
    public function testCalculatesSalaryBonus(
        int $constantSalaryBonusAmount,
        int $yearsOfService,
        int $expectedResult
    ): void {
        //Given
        $longServiceBonusCalculator = new ConstantSalaryBonusCalculator(
            YearsOfService::create($yearsOfService),
            ConstantSalaryBonus::create($constantSalaryBonusAmount)
        );

        //When
        $salaryBonus = $longServiceBonusCalculator->calculate();

        //Then
        self::assertSame($expectedResult, $salaryBonus->getAmount());
    }

    /**
     * @return array<mixed>
     */
    public function inputDataProvider(): array
    {
        return [
            [10000, 5, 5 * 10000], //standard calculation
            [10000, 11, 10 * 10000], //calculating bonus only for fist 10 years of service
            [10000, 0, 0], //employment is shorter than 1 full year
        ];
    }
}
