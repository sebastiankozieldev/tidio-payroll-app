<?php

declare(strict_types=1);

namespace Tests\Unit\Domain\Payroll\Calculator;

use App\Domain\Common\Model\{BaseSalary, PercentageOfSalaryBonus, PercentageSalaryBonusThreshold, YearsOfService};
use App\Domain\Payroll\Calculator\PercentageSalaryBonusCalculator;
use Tests\UnitTestCase;

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
final class PercentageBonusCalculatorTest extends UnitTestCase
{
    private const PERCENTAGE_SALARY_BONUS_THRESHOLD_IN_YEARS = 5;

    /**
     * @dataProvider inputDataProvider
     */
    public function testCalculatesSalaryBonus(
        int $baseSalaryAmount,
        int $yearsOfService,
        float $percentageOfSalaryBonus,
        int $expectedResult
    ): void {
        //Given
        $percentageBonusCalculator = new PercentageSalaryBonusCalculator(
            BaseSalary::create($baseSalaryAmount),
            YearsOfService::create($yearsOfService),
            PercentageOfSalaryBonus::create($percentageOfSalaryBonus),
            PercentageSalaryBonusThreshold::create(self::PERCENTAGE_SALARY_BONUS_THRESHOLD_IN_YEARS)
        );

        //When
        $salaryBonus = $percentageBonusCalculator->calculate();

        //Then
        self::assertSame($expectedResult, $salaryBonus->getAmount());
    }

    /**
     * @return array<mixed>
     */
    public function inputDataProvider(): array
    {
        return [
            [10000, 3, 10.0, 0], //when years of service are not high enough
            [10000, 5, 10.0, (int) (10000 * 10.0 / 100)], //when years of service meets threshold
            [10000, 6, 5.5, (int) (10000 * 5.5 / 100)], //when years of service overcomes threshold
        ];
    }
}
