<?php

declare(strict_types=1);

namespace Tests\Feature\Payroll;

use App\Infrastructure\Laravel\Models\{Department, Employee};
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
final class PayrollFilterTest extends BasePayrollTest
{
    public function testReturnsPayrollFilteredByDepartmentName(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->create([
                'name' => 'HR Department',
            ]);

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->create([
                'name' => 'Dev Department',
            ]);

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->create([
                'name' => 'QA Department',
            ]);

        //When
        $response = $this->getJson(sprintf('%s?department_name=QA Department', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 1)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('department_name', 'QA Department')->etc();
                    });
            });
    }

    public function testReturnsUnfilteredPayrollWhenByDepartmentNameFilterIsEmpty(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->create([
                'name' => 'HR Department',
            ]);

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->create([
                'name' => 'Dev Department',
            ]);

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->create([
                'name' => 'QA Department',
            ]);

        //When
        $response = $this->getJson(sprintf('%s?department_name=', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3);
            });
    }

    public function testReturnsPayrollFilteredByFirstName(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'first_name' => 'Bogusław',
            ]))
            ->create();

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'first_name' => 'Domino',
            ]))
            ->create();

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'first_name' => 'Kamil',
            ]))
            ->create();

        //When
        $response = $this->getJson(sprintf('%s?first_name=Bogusław', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 1)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('first_name', 'Bogusław')->etc();
                    });
            });
    }

    public function testReturnsUnfilteredPayrollWhenByFirstNameFilterIsEmpty(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'first_name' => 'Bogusław',
            ]))
            ->create();

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'first_name' => 'Domino',
            ]))
            ->create();

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'first_name' => 'Kamil',
            ]))
            ->create();

        //When
        $response = $this->getJson(sprintf('%s?first_name=', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3);
            });
    }

    public function testReturnsPayrollFilteredByLastName(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'last_name' => 'Łęcina',
            ]))
            ->create();

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'last_name' => 'Jachaś',
            ]))
            ->create();

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'last_name' => 'Zdun',
            ]))
            ->create();

        //When
        $response = $this->getJson(sprintf('%s?last_name=Łęcina', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 1)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('last_name', 'Łęcina')->etc();
                    });
            });
    }

    public function testReturnsUnfilteredPayrollWhenByLastNameFilterIsEmpty(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'last_name' => 'Łęcina',
            ]))
            ->create();

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'last_name' => 'Jachaś',
            ]))
            ->create();

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'last_name' => 'Zdun',
            ]))
            ->create();

        //When
        $response = $this->getJson(sprintf('%s?last_name=', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3);
            });
    }
}
