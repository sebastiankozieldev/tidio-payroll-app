<?php

declare(strict_types=1);

namespace Tests\Feature\Payroll;

use App\Infrastructure\Laravel\Models\{Department, Employee};
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
final class PayrollSortTest extends BasePayrollTest
{
    public function testReturnsPayrollSortedByFirstNameAsc(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'first_name' => 'Bogusław',
            ]))
            ->has(Employee::factory()->state([
                'first_name' => 'Domino',
            ]))
            ->has(Employee::factory()->state([
                'first_name' => 'Kamil',
            ]))
            ->create();

        //When
        $response = $this->getJson(sprintf('%s?order_by=first_name', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('first_name', 'Bogusław')->etc();
                    })
                    ->has('data.1', function (AssertableJson $json): void {
                        $json->where('first_name', 'Domino')->etc();
                    })
                    ->has('data.2', function (AssertableJson $json): void {
                        $json->where('first_name', 'Kamil')->etc();
                    });
            });
    }

    public function testReturnsPayrollSortedByFirstNameDesc(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'first_name' => 'Bogusław',
            ]))
            ->has(Employee::factory()->state([
                'first_name' => 'Domino',
            ]))
            ->has(Employee::factory()->state([
                'first_name' => 'Kamil',
            ]))
            ->create();

        //When
        $response = $this->getJson(sprintf('%s?order_by=first_name&order_direction=desc', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('first_name', 'Kamil')->etc();
                    })
                    ->has('data.1', function (AssertableJson $json): void {
                        $json->where('first_name', 'Domino')->etc();
                    })
                    ->has('data.2', function (AssertableJson $json): void {
                        $json->where('first_name', 'Bogusław')->etc();
                    });
            });
    }

    public function testReturnsPayrollSortedByLastNameAsc(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'last_name' => 'Łęcina',
            ]))
            ->has(Employee::factory()->state([
                'last_name' => 'Jachaś',
            ]))
            ->has(Employee::factory()->state([
                'last_name' => 'Zdun',
            ]))
            ->create();

        //When
        $response = $this->getJson(sprintf('%s?order_by=last_name', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('last_name', 'Jachaś')->etc();
                    })
                    ->has('data.1', function (AssertableJson $json): void {
                        $json->where('last_name', 'Łęcina')->etc();
                    })
                    ->has('data.2', function (AssertableJson $json): void {
                        $json->where('last_name', 'Zdun')->etc();
                    });
            });
    }

    public function testReturnsPayrollSortedByLastNameDesc(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'last_name' => 'Łęcina',
            ]))
            ->has(Employee::factory()->state([
                'last_name' => 'Jachaś',
            ]))
            ->has(Employee::factory()->state([
                'last_name' => 'Zdun',
            ]))
            ->create();

        //When
        $response = $this->getJson(sprintf('%s?order_by=last_name&order_direction=desc', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('last_name', 'Zdun')->etc();
                    })
                    ->has('data.1', function (AssertableJson $json): void {
                        $json->where('last_name', 'Łęcina')->etc();
                    })
                    ->has('data.2', function (AssertableJson $json): void {
                        $json->where('last_name', 'Jachaś')->etc();
                    });
            });
    }

    public function testReturnsPayrollSortedByDepartmentNameAsc(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->create([
                'name' => 'HR',
            ]);

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->create([
                'name' => 'Dev',
            ]);

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->create([
                'name' => 'QA',
            ]);

        //When
        $response = $this->getJson(sprintf('%s?order_by=department_name', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('department_name', 'Dev')->etc();
                    })
                    ->has('data.1', function (AssertableJson $json): void {
                        $json->where('department_name', 'HR')->etc();
                    })
                    ->has('data.2', function (AssertableJson $json): void {
                        $json->where('department_name', 'QA')->etc();
                    });
            });
    }

    public function testReturnsPayrollSortedByDepartmentNameDesc(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->create([
                'name' => 'HR',
            ]);

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->create([
                'name' => 'Dev',
            ]);

        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->create([
                'name' => 'QA',
            ]);

        //When
        $response = $this->getJson(sprintf('%s?order_by=department_name&order_direction=desc', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('department_name', 'QA')->etc();
                    })
                    ->has('data.1', function (AssertableJson $json): void {
                        $json->where('department_name', 'HR')->etc();
                    })
                    ->has('data.2', function (AssertableJson $json): void {
                        $json->where('department_name', 'Dev')->etc();
                    });
            });
    }

    public function testReturnsPayrollSortedByBaseSalaryAsc(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'base_salary' => 300000,
            ]))
            ->has(Employee::factory()->state([
                'base_salary' => 200000,
            ]))
            ->has(Employee::factory()->state([
                'base_salary' => 100000,
            ]))
            ->create();

        //When
        $response = $this->getJson(sprintf('%s?order_by=base_salary', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('base_salary', '1000.00')->etc();
                    })
                    ->has('data.1', function (AssertableJson $json): void {
                        $json->where('base_salary', '2000.00')->etc();
                    })
                    ->has('data.2', function (AssertableJson $json): void {
                        $json->where('base_salary', '3000.00')->etc();
                    });
            });
    }

    public function testReturnsPayrollSortedByBaseSalaryDesc(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'base_salary' => 300000,
            ]))
            ->has(Employee::factory()->state([
                'base_salary' => 200000,
            ]))
            ->has(Employee::factory()->state([
                'base_salary' => 100000,
            ]))
            ->create();

        //When
        $response = $this->getJson(sprintf('%s?order_by=base_salary&order_direction=desc', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('base_salary', '3000.00')->etc();
                    })
                    ->has('data.1', function (AssertableJson $json): void {
                        $json->where('base_salary', '2000.00')->etc();
                    })
                    ->has('data.2', function (AssertableJson $json): void {
                        $json->where('base_salary', '1000.00')->etc();
                    });
            });
    }

    public function testReturnsPayrollSortedBySalaryBonusAsc(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'employment_started_at' => self::yearsOfServiceToDateTime(3), //300.00 salary bonus
            ]))
            ->has(Employee::factory()->state([
                'employment_started_at' => self::yearsOfServiceToDateTime(5), //500.00 salary bonus
            ]))
            ->has(Employee::factory()->state([
                'employment_started_at' => self::yearsOfServiceToDateTime(10), //1000.00 salary bonus
            ]))
            ->create([
                'constant_salary_bonus' => 10000, //100.00 for each year
            ]);

        //When
        $response = $this->getJson(sprintf('%s?order_by=salary_bonus', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('salary_bonus', '300.00')->etc();
                    })
                    ->has('data.1', function (AssertableJson $json): void {
                        $json->where('salary_bonus', '500.00')->etc();
                    })
                    ->has('data.2', function (AssertableJson $json): void {
                        $json->where('salary_bonus', '1000.00')->etc();
                    });
            });
    }

    public function testReturnsPayrollSortedBySalaryBonusDesc(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'employment_started_at' => self::yearsOfServiceToDateTime(3), //300.00 salary bonus
            ]))
            ->has(Employee::factory()->state([
                'employment_started_at' => self::yearsOfServiceToDateTime(5), //500.00 salary bonus
            ]))
            ->has(Employee::factory()->state([
                'employment_started_at' => self::yearsOfServiceToDateTime(10), //1000.00 salary bonus
            ]))
            ->create([
                'constant_salary_bonus' => 10000, //100.00 for each year
            ]);

        //When
        $response = $this->getJson(sprintf('%s?order_by=salary_bonus&order_direction=desc', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('salary_bonus', '1000.00')->etc();
                    })
                    ->has('data.1', function (AssertableJson $json): void {
                        $json->where('salary_bonus', '500.00')->etc();
                    })
                    ->has('data.2', function (AssertableJson $json): void {
                        $json->where('salary_bonus', '300.00')->etc();
                    });
            });
    }

    public function testReturnsPayrollSortedBySalaryBonusTypeAsc(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->count(2)
            ->create();

        Department::factory()->percentageSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->count(2)
            ->create();

        //When
        $response = $this->getJson(sprintf('%s?order_by=salary_bonus_type', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 4)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('salary_bonus_type', 'constant')->etc();
                    })
                    ->has('data.1', function (AssertableJson $json): void {
                        $json->where('salary_bonus_type', 'constant')->etc();
                    })
                    ->has('data.2', function (AssertableJson $json): void {
                        $json->where('salary_bonus_type', 'percentage')->etc();
                    })
                    ->has('data.3', function (AssertableJson $json): void {
                        $json->where('salary_bonus_type', 'percentage')->etc();
                    });
            });
    }

    public function testReturnsPayrollSortedBySalaryBonusTypeDesc(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->count(2)
            ->create();

        Department::factory()->percentageSalaryBonus()
            ->has(Employee::factory()->count(1))
            ->count(2)
            ->create();

        //When
        $response = $this->getJson(sprintf('%s?order_by=salary_bonus_type&order_direction=desc', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 4)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('salary_bonus_type', 'percentage')->etc();
                    })
                    ->has('data.1', function (AssertableJson $json): void {
                        $json->where('salary_bonus_type', 'percentage')->etc();
                    })
                    ->has('data.2', function (AssertableJson $json): void {
                        $json->where('salary_bonus_type', 'constant')->etc();
                    })
                    ->has('data.3', function (AssertableJson $json): void {
                        $json->where('salary_bonus_type', 'constant')->etc();
                    });
            });
    }

    public function testReturnsPayrollSortedByTotalSalaryAsc(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'base_salary' => 300000, //3000.00
                'employment_started_at' => self::yearsOfServiceToDateTime(3), //300.00 salary bonus
            ]))
            ->has(Employee::factory()->state([
                'base_salary' => 200000, //2000.00
                'employment_started_at' => self::yearsOfServiceToDateTime(5), //500.00 salary bonus
            ]))
            ->has(Employee::factory()->state([
                'base_salary' => 100000, //1000.00
                'employment_started_at' => self::yearsOfServiceToDateTime(10), //1000.00 salary bonus
            ]))
            ->create([
                'constant_salary_bonus' => 10000, //100.00 for each year
            ]);

        //When
        $response = $this->getJson(sprintf('%s?order_by=total_salary', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('base_salary', '1000.00')
                            ->where('salary_bonus', '1000.00')
                            ->where('total_salary', '2000.00')
                            ->etc();
                    })
                    ->has('data.1', function (AssertableJson $json): void {
                        $json->where('base_salary', '2000.00')
                            ->where('salary_bonus', '500.00')
                            ->where('total_salary', '2500.00')
                            ->etc();
                    })
                    ->has('data.2', function (AssertableJson $json): void {
                        $json->where('base_salary', '3000.00')
                            ->where('salary_bonus', '300.00')
                            ->where('total_salary', '3300.00')
                            ->etc();
                    });
            });
    }

    public function testReturnsPayrollSortedByTotalSalaryDesc(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'base_salary' => 300000, //3000.00
                'employment_started_at' => self::yearsOfServiceToDateTime(3), //300.00 salary bonus
            ]))
            ->has(Employee::factory()->state([
                'base_salary' => 200000, //2000.00
                'employment_started_at' => self::yearsOfServiceToDateTime(5), //500.00 salary bonus
            ]))
            ->has(Employee::factory()->state([
                'base_salary' => 100000, //1000.00
                'employment_started_at' => self::yearsOfServiceToDateTime(10), //1000.00 salary bonus
            ]))
            ->create([
                'constant_salary_bonus' => 10000, //100.00 for each year
            ]);

        //When
        $response = $this->getJson(sprintf('%s?order_by=total_salary&order_direction=desc', self::ENDPOINT));

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 3)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('base_salary', '3000.00')
                            ->where('salary_bonus', '300.00')
                            ->where('total_salary', '3300.00')
                            ->etc();
                    })
                    ->has('data.1', function (AssertableJson $json): void {
                        $json->where('base_salary', '2000.00')
                            ->where('salary_bonus', '500.00')
                            ->where('total_salary', '2500.00')
                            ->etc();
                    })
                    ->has('data.2', function (AssertableJson $json): void {
                        $json->where('base_salary', '1000.00')
                            ->where('salary_bonus', '1000.00')
                            ->where('total_salary', '2000.00')
                            ->etc();
                    });
            });
    }
}
