<?php

declare(strict_types=1);

namespace Tests\Feature\Payroll;

use Carbon\CarbonImmutable;
use Tests\FeatureTestCase;

abstract class BasePayrollTest extends FeatureTestCase
{
    protected const ENDPOINT = '/api/payroll';

    protected static function yearsOfServiceToDateTime(int $yearsOfService): CarbonImmutable
    {
        return self::createDateTime()->subYears($yearsOfService);
    }
}
