<?php

declare(strict_types=1);

namespace Tests\Feature\Payroll;

use App\Infrastructure\Laravel\Models\{Department, Employee};
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
final class PayrollTest extends BasePayrollTest
{
    public function testReturnsPayrollWithEmployeeWithConstantSalaryBonus(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'first_name' => 'Bogusław',
                'last_name' => 'Łęcina',
                'base_salary' => 200000,
                'employment_started_at' => self::yearsOfServiceToDateTime(3),
            ]))
            ->create([
                'name' => 'HR',
                'constant_salary_bonus' => 10000,
            ]);

        //When
        $response = $this->getJson(self::ENDPOINT);

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 1)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('first_name', 'Bogusław')
                            ->where('last_name', 'Łęcina')
                            ->where('department_name', 'HR')
                            ->where('base_salary', '2000.00')
                            ->where('salary_bonus', '300.00') //100 for each year
                            ->where('salary_bonus_type', 'constant')
                            ->where('total_salary', '2300.00');
                    });
            });
    }

    public function testReturnsPayrollWithEmployeeWithConstantSalaryBonusOnlyForFirst10YearsOfService(): void
    {
        //Given
        Department::factory()->constantSalaryBonus()
            ->has(Employee::factory()->state([
                'first_name' => 'Bogusław',
                'last_name' => 'Łęcina',
                'base_salary' => 200000,
                'employment_started_at' => self::yearsOfServiceToDateTime(12),
            ]))
            ->create([
                'name' => 'HR',
                'constant_salary_bonus' => 10000,
            ]);

        //When
        $response = $this->getJson(self::ENDPOINT);

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 1)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('first_name', 'Bogusław')
                            ->where('last_name', 'Łęcina')
                            ->where('department_name', 'HR')
                            ->where('base_salary', '2000.00')
                            ->where('salary_bonus', '1000.00') //100 for each year, but max 10 years
                            ->where('salary_bonus_type', 'constant')
                            ->where('total_salary', '3000.00');
                    });
            });
    }

    public function testReturnsPayrollWithEmployeeWithPercentageSalaryBonus(): void
    {
        //Given
        Department::factory()->percentageSalaryBonus()
            ->has(Employee::factory()->state([
                'first_name' => 'Bogusław',
                'last_name' => 'Łęcina',
                'base_salary' => 200000,
                'employment_started_at' => self::yearsOfServiceToDateTime(3),
            ]))
            ->create([
                'name' => 'HR',
                'percentage_of_salary_bonus' => 10.0,
                'percentage_salary_bonus_threshold' => 3,
            ]);

        //When
        $response = $this->getJson(self::ENDPOINT);

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 1)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('first_name', 'Bogusław')
                            ->where('last_name', 'Łęcina')
                            ->where('department_name', 'HR')
                            ->where('base_salary', '2000.00')
                            ->where('salary_bonus', '200.00') //10% of 2000.00
                            ->where('salary_bonus_type', 'percentage')
                            ->where('total_salary', '2200.00');
                    });
            });
    }

    public function testReturnsPayrollWithEmployeeWithPercentageSalaryBonusBeforeAchievingRequiredYearsOfService(): void
    {
        //Given
        Department::factory()->percentageSalaryBonus()
            ->has(Employee::factory()->state([
                'first_name' => 'Bogusław',
                'last_name' => 'Łęcina',
                'base_salary' => 200000,
                'employment_started_at' => self::yearsOfServiceToDateTime(3),
            ]))
            ->create([
                'name' => 'HR',
                'percentage_of_salary_bonus' => 10.0,
                'percentage_salary_bonus_threshold' => 10,
            ]);

        //When
        $response = $this->getJson(self::ENDPOINT);

        //Then
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(function (AssertableJson $json): void {
                $json->has('data', 1)
                    ->has('data.0', function (AssertableJson $json): void {
                        $json->where('first_name', 'Bogusław')
                            ->where('last_name', 'Łęcina')
                            ->where('department_name', 'HR')
                            ->where('base_salary', '2000.00')
                            ->where(
                                'salary_bonus',
                                '0.00'
                            ) //department requires 10 years of service, but employee has only 3
                            ->where('salary_bonus_type', 'percentage')
                            ->where('total_salary', '2000.00');
                    });
            });
    }
}
