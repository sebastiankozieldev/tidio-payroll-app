<?php

declare(strict_types=1);

namespace Tests\Feature\Commands;

use App\Infrastructure\Laravel\Models\Department;
use Tests\FeatureTestCase;

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
final class CreateDepartmentCommandTest extends FeatureTestCase
{
    public function testCreatesDepartmentWithConstantSalaryBonus(): void
    {
        //Given
        $departmentName = 'constant_department_name';

        self::assertFalse(Department::whereName($departmentName)->exists());

        //When
        $this->artisan('department:create')
            ->expectsQuestion('Department name? e.g. HR', $departmentName)
            ->expectsChoice('Salary bonus type?', 'constant', ['constant', 'percentage'])
            ->expectsQuestion('Salary bonus? e.g. 199.99', '100.00')
            ->expectsOutput('Department has been created')
            ->assertExitCode(0);

        //Then
        /** @var Department $department */
        $department = Department::whereName($departmentName)->first();
        self::assertSame($departmentName, $department->name);
        self::assertSame('constant', $department->salary_bonus_type);
        self::assertSame(10000, $department->constant_salary_bonus);
        self::assertNull($department->percentage_of_salary_bonus);
        self::assertNull($department->percentage_salary_bonus_threshold);
    }

    public function testCreatesDepartmentWithPercentageSalaryBonus(): void
    {
        //Given
        $departmentName = 'percentage_department_name';

        //Then
        self::assertFalse(Department::whereName($departmentName)->exists());

        //When
        $this->artisan('department:create')
            ->expectsQuestion('Department name? e.g. HR', $departmentName)
            ->expectsChoice('Salary bonus type?', 'percentage', ['constant', 'percentage'])
            ->expectsQuestion('Percentage of salary bonus? e.g. 5.5', '10')
            ->expectsQuestion('Required years of service to receive salary bonus? e.g. 10', '5')
            ->expectsOutput('Department has been created')
            ->assertExitCode(0);

        //Then
        /** @var Department $department */
        $department = Department::whereName($departmentName)->first();
        self::assertSame($departmentName, $department->name);
        self::assertSame('percentage', $department->salary_bonus_type);
        self::assertNull($department->constant_salary_bonus);
        self::assertSame(10.0, $department->percentage_of_salary_bonus);
        self::assertSame(5, $department->percentage_salary_bonus_threshold);
    }
}
