<?php

declare(strict_types=1);

namespace Tests\Feature\Commands;

use App\Infrastructure\Laravel\Models\{Department, Employee};
use Tests\FeatureTestCase;

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
final class CreateEmployeeCommandTest extends FeatureTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        Department::factory()->constantSalaryBonus()->create([
            'name' => 'Test Department A',
        ]);
        Department::factory()->constantSalaryBonus()->create([
            'name' => 'Test Department B',
        ]);
        Department::factory()->constantSalaryBonus()->create([
            'name' => 'Test Department C',
        ]);
    }

    public function testCreatesEmployee(): void
    {
        //When
        $this->artisan('employee:create')
            ->expectsQuestion('First name?', 'Bogusław')
            ->expectsQuestion('Last name?', 'Łęcina')
            ->expectsQuestion('Base salary? e.g. 2000.00', '2000.00')
            ->expectsQuestion('Employment start date? (dd-mm-yyyy)', '20-01-2020')
            ->expectsChoice(
                'Department?',
                'Test Department B',
                ['Test Department A', 'Test Department B', 'Test Department C']
            )
            ->expectsOutput('Employee has been created')
            ->assertExitCode(0);

        //Then
        /** @var Employee $employee */
        $employee = Employee::where([
            'first_name' => 'Bogusław', 'last_name' => 'Łęcina',
        ])->first();

        self::assertSame('Bogusław', $employee->first_name);
        self::assertSame('Łęcina', $employee->last_name);
        self::assertSame(200000, $employee->base_salary);
        self::assertSame('20-01-2020', $employee->employment_started_at->format('d-m-Y'));
        self::assertSame('Test Department B', $employee->department->name);
    }
}
