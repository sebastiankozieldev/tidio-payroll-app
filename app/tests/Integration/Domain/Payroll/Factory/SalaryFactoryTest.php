<?php

declare(strict_types=1);

namespace Tests\Integration\Domain\Payroll\Factory;

use App\Domain\Common\Model\{BaseSalary, ConstantSalaryBonus, PercentageOfSalaryBonus, PercentageSalaryBonusThreshold};
use App\Domain\Department\Model\SalaryBonusType;
use App\Domain\Payroll\Factory\SalaryFactory;
use App\Utils\DateTime\DateTimeProvider;
use PHPUnit\Framework\MockObject\Stub;
use Tests\IntegrationTestCase;

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
final class SalaryFactoryTest extends IntegrationTestCase
{
    private SalaryFactory $salaryFactory;

    private DateTimeProvider|Stub $dateTimeProvider;

    protected function setUp(): void
    {
        parent::setUp();
        $this->dateTimeProvider = $this->createStub(DateTimeProvider::class);
        $this->dateTimeProvider->method('now')
            ->willReturn(self::createDateTime()->toDateTimeImmutable());

        $this->salaryFactory = new SalaryFactory($this->dateTimeProvider);
    }

    public function testMakesSalaryWithConstantSalaryBonus(): void
    {
        //Given
        $baseSalaryAmount = 100000; //1000.00
        $baseSalary = BaseSalary::create($baseSalaryAmount);
        $yearsOfService = 3;
        $employmentStartDate = self::createDateTime()->subYears($yearsOfService)->toDateTimeImmutable();
        $constantSalaryBonusAmount = 10000; //100.00
        $constantSalaryBonus = ConstantSalaryBonus::create($constantSalaryBonusAmount);

        //When
        $salary = $this->salaryFactory->makeSalaryWithConstantSalaryBonus(
            $baseSalary,
            $employmentStartDate,
            $constantSalaryBonus
        );

        //Then
        self::assertSame($baseSalaryAmount, $salary->getBaseSalary()->getAmount());
        self::assertSame(SalaryBonusType::CONSTANT, $salary->getSalaryBonusType());
        self::assertSame($yearsOfService * $constantSalaryBonusAmount, $salary->getSalaryBonus()->getAmount());
        self::assertSame($baseSalaryAmount + $yearsOfService * $constantSalaryBonusAmount, $salary->getAmount());
    }

    public function testMakeSalaryWithPercentageSalaryBonus(): void
    {
        //Given
        $baseSalaryAmount = 100000; //1000.00
        $baseSalary = BaseSalary::create($baseSalaryAmount);
        $yearsOfService = 3;
        $employmentStartDate = self::createDateTime()->subYears($yearsOfService)->toDateTimeImmutable();
        $percentageOfSalaryBonusValue = 5.0;
        $percentageOfSalaryBonus = PercentageOfSalaryBonus::create($percentageOfSalaryBonusValue);
        $percentageSalaryBonusThreshold = PercentageSalaryBonusThreshold::create(1);

        //When
        $salary = $this->salaryFactory->makeSalaryWithPercentageSalaryBonus(
            $baseSalary,
            $employmentStartDate,
            $percentageOfSalaryBonus,
            $percentageSalaryBonusThreshold
        );

        //Then
        $expectedSalaryBonus = (int) ($percentageOfSalaryBonusValue * $baseSalaryAmount / 100);
        self::assertSame($baseSalaryAmount, $salary->getBaseSalary()->getAmount());
        self::assertSame(SalaryBonusType::PERCENTAGE, $salary->getSalaryBonusType());
        self::assertSame($expectedSalaryBonus, $salary->getSalaryBonus()->getAmount());
        self::assertSame($baseSalaryAmount + $expectedSalaryBonus, $salary->getAmount());
    }
}
