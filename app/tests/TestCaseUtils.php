<?php

declare(strict_types=1);

namespace Tests;

use App\Utils\DateTime\ConstantDateTimeProvider;
use Carbon\CarbonImmutable;
use Ramsey\Uuid\Uuid;

trait TestCaseUtils
{
    protected static function createDateTime(): CarbonImmutable
    {
        return new CarbonImmutable(ConstantDateTimeProvider::CONSTANT_DATE_TIME);
    }

    protected static function createUuid(): string
    {
        return (string) Uuid::uuid4();
    }
}
