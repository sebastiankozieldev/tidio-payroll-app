<?php

declare(strict_types=1);

namespace Tests;

use App\Utils\DateTime\{ConstantDateTimeProvider, DateTimeProvider};
use Illuminate\Foundation\Testing\{RefreshDatabase, TestCase as BaseTestCase};

abstract class FeatureTestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;
    use TestCaseUtils;

    protected function setUp(): void
    {
        parent::setUp();
        $this->instance(DateTimeProvider::class, new ConstantDateTimeProvider());
    }
}
