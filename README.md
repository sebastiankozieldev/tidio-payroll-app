# Tidio Payroll App #

### Requirements
* [Docker](https://docs.docker.com/) >= 20.10.2
* [Docker Compose](https://docs.docker.com/compose/) >= 18.06
* [make](https://www.gnu.org/software/make/manual/make.html)
* Tested on macOS 12 with Intel processor, but should start in other environments

### First Setup
* ``cp -n ./.env.dist ./.env || :`` - copy default ``.env`` file
* ``make setup`` - setup project with this command. App will be available locally on port defined in the `HOST_WEB_APP_PORT` env variable, `80` by default.
* ``make db-seed`` (optional) - seed database with example values

### API Documentation
This repository contains OpenAPI documentation in the ``open_api_documentation.yaml`` file.
Open it with any OpenAPI 3.0.1 compatible editor, e.g. [Swagger Editor](https://editor.swagger.io/)

### Laravel commands
After entering the php container with ``make bash`` command, you can run laravel commands.
#### Available commands
* ``php artisan department:create`` - asks for all required information to create a new department
* ``php artisan employee:create`` - asks for all required information to create a new employee

### Make commands overview
All commands should be executed from root directory of this project

* ``make start`` - starts all docker containers
* ``make stop`` - stops all docker containers
* ``make down`` - brings down all docker containers and removes volumes. Warning, without `02-db-volume.yml` compose file, it removes database container with all data
* ``make build`` - builds docker images
* ``make bash`` - enters php container and allows to run any php command from there
* ``make status`` - displays running docker containers
* ``make logs`` - follows php container logs
* ``make setup`` - prepares application for first run,
copies all required files from dists, builds images, installs dependencies,starts containers and runs migrations
* ``make code-quality`` - runs all code quality assurance tools
* ``make phpstan`` - runs phpstan
* ``make ecs`` - runs easy coding standard check
* ``make ecs-fix`` - fixes code with easy coding standard
* ``make deptrac`` - checks code layers segregation
* ``make db-reset`` - reruns all migrations
* ``make db-seed`` - seeds database with example values. Seeders do not append values.
Run ``make db-reset`` before running seeders again

### Tests
* ``make unit`` - runs unit tests
* ``make integration`` - runs integration tests
* ``make feature`` - runs feature tests
* ``make tests`` - runs all tests

### Env variables overview
This section covers env variables related to application setup. The Laravel framework envs are not covered here.

* ``COMPOSE_FILE`` - the app uses multi compose file setup. By default, app starts with all required compose files.
If there is any need for extension, you can create new compose file in the ``./docker/compose`` directory and append it to this variable.
* ``COMPOSE_PROJECT_NAME`` - prefix added to name of each docker container
* ``HOST_WEB_APP_PORT`` - web app port exposed to host, `80` by default. Use it to communicate with application using HTTP  
* ``DATABASE_ROOT_PASSWORD`` - database root user password
* ``DATABASE_NAME`` - database name
* ``DATABASE_USER`` - database user
* ``DATABASE_PASSWORD`` - database password
* ``HOST_DATABASE_PORT`` - database port exposed to host, `3306` by default.
You can use it to connect to database from host machine.

### Compose files overview
* ``00-base.yml`` - base file declaring all required containers (required)
* ``01-ports.yml`` - file containing all ports exposed to host machine (required)
* ``02-db-volume.yml`` - file containing database volume mounted to host machine,
it allows to safely remove `database` container without loosing data (optional)
* ``99-dev.yml`` - file containing all extensions required during development (optional)