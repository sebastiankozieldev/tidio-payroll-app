#!/usr/bin/env make

-include .env
export

default: bash

start:
	docker-compose up -d

stop:
	docker-compose stop

down:
	docker-compose down -v --remove-orphans

build:
	docker-compose build --no-cache

bash:
	docker-compose up -d php
	docker-compose exec php bash

status:
	docker-compose ps

logs:
	docker-compose logs -f php

setup:
	@make build
	@make start
	docker-compose exec -T php composer install
	docker-compose exec -T php bash /wait-for-it.sh "${DB_HOST}:${DB_PORT}"
	docker-compose exec -T php php artisan migrate -n

code-quality:
	@make start
	@make ecs
	@make phpstan
	@make deptrac

phpstan:
	docker-compose exec -T php ./vendor/bin/phpstan analyse --memory-limit=512M

ecs:
	docker-compose exec -T php ./vendor/bin/ecs check

ecs-fix:
	docker-compose exec -T php ./vendor/bin/ecs check --fix

deptrac:
	docker-compose exec -T php ./vendor/bin/deptrac analyze --config-file=/opt/app/deptrac.yml

db-reset:
	docker-compose exec -T php php artisan migrate:refresh

db-seed:
	docker-compose exec -T php php artisan db:seed

tests:
	@make unit
	@make integration
	@make feature

unit:
	docker-compose exec -T php ./vendor/bin/phpunit --testsuite Unit

integration:
	docker-compose exec -T php ./vendor/bin/phpunit --testsuite Integration

feature:
	docker-compose exec -T php ./vendor/bin/phpunit --testsuite Feature